var mix    = require('laravel-mix');
var fs     = require('fs');
var is_dir = function(path) { 
    try {
	    fs.statSync(path);
	    return true;
    } catch(e) {
	    return false;
    }
};

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

var items = ['css','js','less','sass'];
 
fs.readdirSync('app/Http/Modules').forEach( function(dirname) {
    items.forEach( function(item) {
        if( is_dir('app/Http/Modules/' + dirname + '/Assets/' + item) ) {
            fs.readdirSync('app/Http/Modules/' + dirname + '/Assets/' + item).forEach( function(filename) {
                if( fs.statSync('app/Http/Modules/' + dirname + '/Assets/' + item + '/' + filename).isFile() ) {
                    switch(item) {
                        case 'js': mix.js('app/Http/Modules/' + dirname + '/Assets/js/' + filename, 'public/js'); break;
                        case 'css': mix.styles('app/Http/Modules/' + dirname + '/Assets/css/' + filename, 'public/css/' + filename); break;
                        case 'sass': mix.sass('app/Http/Modules/' + dirname + '/Assets/sass/' + filename, 'public/css'); break;
                        case 'less': mix.less('app/Http/Modules/' + dirname + '/Assets/less/' + filename, 'public/css'); break;
                    }
                }
            });
        }
    });
});

mix.combine([
    'resources/assets/js/html5shiv.js',
    'resources/assets/js/ie8-responsive-file-warning.js',
    'resources/assets/js/jquery-3.1.1.min.js',
    'resources/assets/js/bootstrap.min.js',
    'resources/assets/js/respond.min.js',
    'resources/assets/js/readmore.min.js',
    'resources/assets/js/bootstrap-datepicker.js'
], 'public/js/scripts.js');

mix.styles([
    'resources/assets/css/bootstrap.css',
    'resources/assets/css/bootstrap-theme.css',
    'resources/assets/css/font-awesome.min.css',
    'resources/assets/css/datepicker.css',
    'resources/assets/css/style.css'
], 'public/css/styles.css');

mix.styles('resources/assets/css/signin.css', 'public/css/signin.css');

//mix.combine('resources/assets/js/*', 'public/js/scripts.js');
//   .combine('resources/assets/css/*', 'public/css/style.css');
