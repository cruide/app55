<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::get('js-core', ['as' => 'jscore'], function() {
    return response()->view('js')->header('Content-Type', 'application/javascript');
});

Route::get('locale/{code}', ['as' => 'locale'], function( $code ) {
    if( empty($code) || !preg_match('/^[a-z]+$/i', $code) || strlen($code) > 2 ) {
        abort(404);
    }

    session()->put('locale', strtolower($code));

    if( array_key_isset('HTTP_REFERER', request()->server()) ) {
        return redirect()->back()->withCookie(
            cookie()->forever('locale', strtolower($code))
        );
    }

    return redirect('/')->withCookie(
        cookie()->forever('locale', strtolower($code))
    );
});

