                <ul class="nav nav-pills pull-right">
                    <li @if( is_route_path('/') ) class="active" @endif>
                        <a href="{{ url('/') }}">
                            <span class="glyphicon glyphicon-home"></span>&nbsp;
                            @ufl('strings.home')
                        </a>
                    </li>

                    @if( is_auth() )
                        @if( is_user_level(50) )
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                            &nbsp;
                            Новости <span class="caret"></span>
                        </a>
                        
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ url('/news') }}">
                                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                    &nbsp;
                                    Новости
                                </a>
                            </li>

                            <li>
                                <a href="{{ url('/news/list') }}">
                                    <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                    &nbsp;
                                    Управление новостями
                                </a>
                            </li>
                        </ul>
                    </li>
                        @else
                    <li @if( is_route_path('/news') ) class="active" @endif>
                        <a href="{{ url('/news') }}">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i>&nbsp;
                            @ufl('strings.news')
                        </a>
                    </li>
                        @endif
                                        
                        @if( auth()->user()->isAdmin() )
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-king"></span>
                            &nbsp;
                            Администрирование <span class="caret"></span>
                        </a>
                        
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ url('/pages/list') }}">
                                    <span class="glyphicon glyphicon-list-alt"></span>
                                    &nbsp;
                                    Страницы
                                </a>
                            </li>
                            
                            <li>
                                <a href="{{ url('/users') }}">
                                    <i class="fa fa-users" aria-hidden="true"></i>
                                    &nbsp;
                                    Пользователи
                                </a>
                            </li>
                            
                            @if( auth()->user()->isSuperUser() )
                            <li>
                                <a href="{{ url('/rules') }}">
                                    <span class="glyphicon glyphicon-lock"></span>
                                    &nbsp;
                                    Правила доступа
                                </a>
                            </li>
                            @endif
                        </ul>
                    </li>
                        @endif
                    
                    <li @if( is_route_path('/profile') ) class="active" @endif>
                        <a href="{{ url('/profile') }}">
                            <i class="fa fa-user"></i>&nbsp;
                            @ufl('strings.profile')
                        </a>
                    </li>

                    <li>
                        <a href="{{ url('/logout') }}">
                            <span class="glyphicon glyphicon-log-out"></span>&nbsp;
                            @ufl('strings.exit')
                        </a>
                    </li>
                    @else
                    <li>
                        <a href="{{ url('/login') }}">
                            <span class="glyphicon glyphicon-log-in"></span>&nbsp;
                            @ufl('strings.enter')
                        </a>
                    </li>
                    @endif
                </ul>

