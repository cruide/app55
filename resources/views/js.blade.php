var _app = {

	url: '{{ $base_url }}',
	fn : {},

	pre: {
		now: function() { return new Date(); },
		stamp: function(){ return _app.pre.now.getTime(); }
	},

	empty: function(mixed_var) {
		if( typeof(mixed_var) == 'undefined' || mixed_var === '' || mixed_var === 0 || mixed_var === '0' || mixed_var === null || mixed_var === false || (_app.is_array(mixed_var) && mixed_var.length === 0) ) {
			return true;
		}

		return false;
	},

	is_array: function(mixed_var) { return ( mixed_var instanceof Array ); },
	implode: function(glue, pieces) { return ((pieces instanceof Array) ? pieces.join(glue) : pieces); },

	is_url: function(url) {
		var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
		if( RegExp.test(url) ) { return true; }
		return false;
	},

	is_email: function(email) {
		var RegExp = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if( RegExp.test(email) ) { return true; }
		return false;
	},

	is_exists: function(selector) {
		if( $(selector).size() > 0 ) {
			return true;
		}

		return false;
	},

	function_exists: function( function_name ) {
		if( typeof function_name == 'string' ) { return ( typeof window[function_name] == 'function' );}
		else { return ( function_name instanceof Function );}
	},

	call_user_func: function (cb) { if( typeof(cb) === 'string' && cb != '' && _app.function_exists(cb) ) { var func = cb + "();"; eval(func);}},

	redirect: function(url) {
		if( typeof(url) != 'undefined' && url != '' && _app.is_url(url) == true ) { window.location.href = url;}
		else { console.log('Incorrect URL redirection: ' + url);}
	},

	ucfirst: function(str) {
		var text  = str;
		var parts = text.split(' '), len = parts.length, i, words = [];

		for(i = 0; i < len; i++) {
			var part  = parts[i];
			var first = part[0].toUpperCase();
			var rest  = part.substring(1, part.length);
			var word  = first + rest;

			words.push(word);
		}

		return words.join(' ');
	},

	confirm: function(msg, title, callback) {

		var modalId = 'app-confirm-box-dialog';

		if( typeof(callback) != 'function' ) { console.log('_app.confirm: incorrect callback'); return false;}
		if( typeof(title) == 'undefined' || title == '' ) { title = 'Вы уверены?'; }

		makeup_callback = function() {
			jQuery(modalId).modal('hide');
			callback();
		};

		var modal = $('<div/>', {
			'class': 'modal fade',
			'id': modalId,
			'role': 'dialog',
			'aria-hidden': 'true',
		}).append(
			$('<div/>', {
				'class': 'modal-dialog',
			}).append(
				$('<div/>', {
					'class': 'modal-content',
				}).append(
					$('<div/>', {
						'class': 'modal-header',
					}).append(
						$('<button/>', {
							'type': 'button',
							'class': 'close',
							'data-dismiss': 'modal',
							'aria-hidden': 'true',
							'html': '&times;',
						})
					). append(
						$('<h5/>', {'class': 'modal-title', 'html': title, 'style': 'font-weight: bolder !important;'})
					)
				).append(
					$('<div/>', {'class': 'modal-body', 'css': {'font-size': '14px'}}).append( $('<p/>', {'html': msg}) )
				).append(
					$('<div/>', {
						'class': 'modal-footer',
					}).append(
						$('<button/>', {
							'type': 'button',
							'class': 'btn btn-primary',
							'data-dismiss': 'modal',
							'html': '@ufl('strings.no')',
						})
					).append(
						$('<button/>', {
							'type': 'button',
							'class': 'btn btn-default',
							'html': '@ufl('strings.yes')',
						}).click(function(){
							makeup_callback();
							jQuery('#' + modalId).modal('hide');
						})
					)
				)
			)
		);

		jQuery('body').append( modal );

		jQuery('#' + modalId).on('hidden.bs.modal', function(){ jQuery('#' + modalId).remove(); });
		jQuery('#' + modalId).modal('show');
	},

	message: function(msg, title, params, callback) {

		var modalId = 'app-message-box-dialog';

		if( typeof(title) == 'undefined' || title == '' ) { title = 'Message'; }

		if( typeof(params) == 'function' && typeof(callback) == 'undefined' ) {
			callback = params;
		} else {
			if( typeof(params) != 'object' ) { params = { width: 300, height: 150 }}
			if( typeof(params.width) == 'undefined' || params.width ==  "" ) params.width = 300;
			if( typeof(params.height) == 'undefined' || params.height ==  "" ) params.height = 300;
		}

		var modal = $('<div/>', {
			'class': 'modal fade',
			'id': modalId,
			'role': 'dialog',
			'aria-hidden': 'true',
			'css': {'z-index': '9999999'},
		}).append(
			$('<div/>', {
				'class': 'modal-dialog',
			}).append(
				$('<div/>', {
					'class': 'modal-content',
				}).append(
					$('<div/>', {
						'class': 'modal-header',
					}).append(
						$('<button/>', {
							'type': 'button',
							'class': 'close',
							'data-dismiss': 'modal',
							'aria-hidden': 'true',
							'html': '&times;',
						})
					). append(
						$('<h5/>', {'class': 'modal-title', 'html': title, 'style': 'font-weight: bolder !important;'})
					)
				).append(
					$('<div/>', {
						'class': 'modal-body',
						'css': {'font-size': '14px'},
					}).append(
						$('<p/>', {
							'html': msg,
						})
					)
				).append(
					$('<div/>', {
						'class': 'modal-footer',
					}).append(
						$('<button/>', {
							'type': 'button',
							'class': 'btn btn-primary',
							'data-dismiss': 'modal',
							'html': '@ufl('strings.close')',
						})
					)
				)
			)
		);

		jQuery('body').append( modal );

		jQuery('#' + modalId).on('hidden.bs.modal', function(){ jQuery('#' + modalId).remove(); if( typeof(callback) == 'function' ) callback(); });
		jQuery('#' + modalId).modal('show');
	},

	preloader: function(sel) {
		if( typeof(jQuery) != 'undefined' && jQuery != ''  ) {
			var now = new Date();

			jQuery(sel).html(
				'<div id="ajax-preloader" style="margin: 0 auto; width: 20px; height: 12px; padding: 5px;">'+
				'<img id="ajax-preloader-image" src="{{ $base_url }}/images/ajax-loader.gif" style="margin: 0 auto; width: 20px; height: 12px;" title="Загрузка данных ..." />'+
				'</div>'
			);
		}
	},

	ajaxGet: function(href, values, callback, json) {
		return this.request.useGet().ajax(href, values, callback);
	},

	ajaxPost: function(href, values, callback, json) {
		return this.request.usePost().ajax(href, values, callback);
	},

	jsonGet: function(href, values, callback) {
		return this.request.useGet().json(href, values, callback, 'json');
	},

	jsonPost: function(href, values, callback) {
		return this.request.usePost().json(href, values, callback, 'json');
	},

	ajaxScriptGet: function(href, values, callback) {
		return this.request.useGet().ajax(href, values, callback, 'script');
	},

	ajaxScriptPost: function(href, values, callback) {
		return this.request.usePost().ajax(href, values, callback, 'script');
	},

	ajaxXmlGet: function(href, values, callback) {
		return this.request.useGet().ajax(href, values, callback, 'xml');
	},

	ajaxXmlPost: function(href, values, callback) {
		return this.request.usePost().ajax(href, values, callback, 'xml');
	},

	request: {
		type: 'POST',
		async: false,

		ajax: function(href, values, callback, response_type) {
			if( !_app.empty(values) && (typeof(values) == 'object' || typeof(values) == 'array') ) {
				if( _app.empty(values['_token']) ) {
					values['_token'] = _app.csrf;
				}

			} else if( !_app.empty(values) && typeof(values) == 'string' ) {
				values = values + '&_token=' + _app.csrf;

			} else if( !_app.empty(values) && typeof(values) == 'function' ) {
				callback = values;
				values   = {'_token': _app.csrf};

			} else {
				values = {'_token': _app.csrf};
			}

			if( typeof(response_type) == 'undefined' || response_type == '' ) {
				response_type = 'html';
			}

			if( response_type != 'html' && response_type != 'script' && response_type != 'json' && response_type != 'xml' ) {
				response_type = 'html';
			}

			jQuery.ajax({
				url: href, type: _app.request.type, dataType: response_type,
				headers: {'X-Request-Name': 'Laravel application request', 'X-Request-Type': response},
				async: _app.request.async, data: values, success: function(result) {
					if( typeof(result._token) != 'undefined' && !_app.empty(result._token) ) {
						_app.csrf = result._token;
					}

					if( response == 'json' && !_app.empty(result) && !_app.empty(result.error) ) {
						_app.message(result.error, '{{ ufl('globals.error') }}!!!');
						return false;

					} else if( response == 'json' && !_app.empty(result) && !_app.empty(result.message) ) {
						_app.message(result.message, '{{ ufl('globals.attention') }}!');
						return false;

					} else if( response == 'json' && !_app.empty(result) && !_app.empty(result.errors) ) {
						var div     = $('<div/>');
						var message = '';
						var counter = 1;

						$.each(result.errors, function(ntx, item) {
							message = message + counter + '. ' + item + '<br />';
							counter++;
						});

						_app.message(message, '{{ ufl('globals.error') }}!', function() { callback(result); });
						return false;
					}

					if( _app.request.async == true ) {
						_app.request.async = false;
					}

					if( typeof(callback) == 'function' ) {
						callback(result);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					if (typeof(console) != 'undefined' && typeof(console) != '') {
						console.log( jqXHR );
						console.log('Request _app.request.ajax to `' + href + '` returned an error ' + textStatus);
						console.log( errorThrown );
					}

					if( _app.request.async == true ) {
						_app.request.async = false;
					}
				},
				timeout: function () {
					if (typeof(console) != 'undefined' && typeof(console) != '') {
						console.log('Request _app.request.ajax to `'+url+'` has timed out');
					}

					if( _app.request.async == true ) {
						_app.request.async = false;
					}
				}
			});

			if( _app.request.type == 'GET' ) {
				_app.request.type = 'POST';
			}
		},

		useGet: function() {
			this.type = 'GET';

			return this;
		},

		usePost: function() {
			this.type = 'POST';

			return this;
		},

		setAsync: function(async) {
			_app.request.async = async;

			return this;
		},

		json: function(href, values, callback) {
			this.ajax(href, values, callback, 'json');
		},
	},

	make_slug: function(s, opt) {
		s   = String(s);
		opt = Object(opt);

		var defaults = {
			'delimiter': '-',
			'limit': undefined,
			'lowercase': true,
			'replacements': {},
			'transliterate': (typeof(XRegExp) === 'undefined') ? true : false
		};

		// Merge options
		for (var k in defaults) {
			if (!opt.hasOwnProperty(k)) {
				opt[k] = defaults[k];
			}
		}

		var char_map = {
			// Latin
			'À': 'A', 'Á': 'A', 'Â': 'A', 'Ã': 'A', 'Ä': 'A', 'Å': 'A', 'Æ': 'AE', 'Ç': 'C',
			'È': 'E', 'É': 'E', 'Ê': 'E', 'Ë': 'E', 'Ì': 'I', 'Í': 'I', 'Î': 'I', 'Ï': 'I',
			'Ð': 'D', 'Ñ': 'N', 'Ò': 'O', 'Ó': 'O', 'Ô': 'O', 'Õ': 'O', 'Ö': 'O', 'Ő': 'O',
			'Ø': 'O', 'Ù': 'U', 'Ú': 'U', 'Û': 'U', 'Ü': 'U', 'Ű': 'U', 'Ý': 'Y', 'Þ': 'TH',
			'ß': 'ss',
			'à': 'a', 'á': 'a', 'â': 'a', 'ã': 'a', 'ä': 'a', 'å': 'a', 'æ': 'ae', 'ç': 'c',
			'è': 'e', 'é': 'e', 'ê': 'e', 'ë': 'e', 'ì': 'i', 'í': 'i', 'î': 'i', 'ï': 'i',
			'ð': 'd', 'ñ': 'n', 'ò': 'o', 'ó': 'o', 'ô': 'o', 'õ': 'o', 'ö': 'o', 'ő': 'o',
			'ø': 'o', 'ù': 'u', 'ú': 'u', 'û': 'u', 'ü': 'u', 'ű': 'u', 'ý': 'y', 'þ': 'th',
			'ÿ': 'y',

			// Latin symbols
			'©': '(c)',

			// Greek
			'Α': 'A', 'Β': 'B', 'Γ': 'G', 'Δ': 'D', 'Ε': 'E', 'Ζ': 'Z', 'Η': 'H', 'Θ': '8',
			'Ι': 'I', 'Κ': 'K', 'Λ': 'L', 'Μ': 'M', 'Ν': 'N', 'Ξ': '3', 'Ο': 'O', 'Π': 'P',
			'Ρ': 'R', 'Σ': 'S', 'Τ': 'T', 'Υ': 'Y', 'Φ': 'F', 'Χ': 'X', 'Ψ': 'PS', 'Ω': 'W',
			'Ά': 'A', 'Έ': 'E', 'Ί': 'I', 'Ό': 'O', 'Ύ': 'Y', 'Ή': 'H', 'Ώ': 'W', 'Ϊ': 'I',
			'Ϋ': 'Y',
			'α': 'a', 'β': 'b', 'γ': 'g', 'δ': 'd', 'ε': 'e', 'ζ': 'z', 'η': 'h', 'θ': '8',
			'ι': 'i', 'κ': 'k', 'λ': 'l', 'μ': 'm', 'ν': 'n', 'ξ': '3', 'ο': 'o', 'π': 'p',
			'ρ': 'r', 'σ': 's', 'τ': 't', 'υ': 'y', 'φ': 'f', 'χ': 'x', 'ψ': 'ps', 'ω': 'w',
			'ά': 'a', 'έ': 'e', 'ί': 'i', 'ό': 'o', 'ύ': 'y', 'ή': 'h', 'ώ': 'w', 'ς': 's',
			'ϊ': 'i', 'ΰ': 'y', 'ϋ': 'y', 'ΐ': 'i',

			// Turkish
			'Ş': 'S', 'İ': 'I', 'Ç': 'C', 'Ü': 'U', 'Ö': 'O', 'Ğ': 'G',
			'ş': 's', 'ı': 'i', 'ç': 'c', 'ü': 'u', 'ö': 'o', 'ğ': 'g',

			// Russian
			'А': 'A', 'Б': 'B', 'В': 'V', 'Г': 'G', 'Д': 'D', 'Е': 'E', 'Ё': 'Yo', 'Ж': 'Zh',
			'З': 'Z', 'И': 'I', 'Й': 'J', 'К': 'K', 'Л': 'L', 'М': 'M', 'Н': 'N', 'О': 'O',
			'П': 'P', 'Р': 'R', 'С': 'S', 'Т': 'T', 'У': 'U', 'Ф': 'F', 'Х': 'H', 'Ц': 'C',
			'Ч': 'Ch', 'Ш': 'Sh', 'Щ': 'Sh', 'Ъ': '', 'Ы': 'Y', 'Ь': '', 'Э': 'E', 'Ю': 'Yu',
			'Я': 'Ya',
			'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'yo', 'ж': 'zh',
			'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o',
			'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'c',
			'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu',
			'я': 'ya',

			// Ukrainian
			'Є': 'Ye', 'І': 'I', 'Ї': 'Yi', 'Ґ': 'G',
			'є': 'ye', 'і': 'i', 'ї': 'yi', 'ґ': 'g',

			// Czech
			'Č': 'C', 'Ď': 'D', 'Ě': 'E', 'Ň': 'N', 'Ř': 'R', 'Š': 'S', 'Ť': 'T', 'Ů': 'U',
			'Ž': 'Z',
			'č': 'c', 'ď': 'd', 'ě': 'e', 'ň': 'n', 'ř': 'r', 'š': 's', 'ť': 't', 'ů': 'u',
			'ž': 'z',

			// Polish
			'Ą': 'A', 'Ć': 'C', 'Ę': 'e', 'Ł': 'L', 'Ń': 'N', 'Ó': 'o', 'Ś': 'S', 'Ź': 'Z',
			'Ż': 'Z',
			'ą': 'a', 'ć': 'c', 'ę': 'e', 'ł': 'l', 'ń': 'n', 'ó': 'o', 'ś': 's', 'ź': 'z',
			'ż': 'z',

			// Latvian
			'Ā': 'A', 'Č': 'C', 'Ē': 'E', 'Ģ': 'G', 'Ī': 'i', 'Ķ': 'k', 'Ļ': 'L', 'Ņ': 'N',
			'Š': 'S', 'Ū': 'u', 'Ž': 'Z',
			'ā': 'a', 'č': 'c', 'ē': 'e', 'ģ': 'g', 'ī': 'i', 'ķ': 'k', 'ļ': 'l', 'ņ': 'n',
			'š': 's', 'ū': 'u', 'ž': 'z'
		};

		// Make custom replacements
		for (var k in opt.replacements) {
			s = s.replace(RegExp(k, 'g'), opt.replacements[k]);
		}

		// Transliterate characters to ASCII
		if (opt.transliterate) {
			for (var k in char_map) {
				s = s.replace(RegExp(k, 'g'), char_map[k]);
			}
		}

		// Replace non-alphanumeric characters with our delimiter
		var alnum = (typeof(XRegExp) === 'undefined') ? RegExp('[^a-z0-9]+', 'ig') : XRegExp('[^\\p{L}\\p{N}]+', 'ig');
		s = s.replace(alnum, opt.delimiter);

		// Remove duplicate delimiters
		s = s.replace(RegExp('[' + opt.delimiter + ']{2,}', 'g'), opt.delimiter);

		// Truncate slug to max. characters
		s = s.substring(0, opt.limit);

		// Remove delimiter from ends
		s = s.replace(RegExp('(^' + opt.delimiter + '|' + opt.delimiter + '$)', 'g'), '');

		return opt.lowercase ? s.toLowerCase() : s;
	},

}

if( typeof(jQuery) != 'function' ) {
	alert( 'Need jQuery...' );
}