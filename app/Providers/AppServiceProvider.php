<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Blade::directive('l', function($expression) {
            return '<?php echo l(' . $expression . '); ?>';
        });

        \Blade::directive('ufl', function($expression) {
            return '<?php echo ufl(' . $expression . '); ?>';
        });

        /**
        * Macros for json response with token integration
        */
        \Response::macro('doJson', function( $value ) {
            if( is_array($value) ) {
                $value['_token'] = csrf_token();

            } else if( is_object($value) ) {
                $value->_token = csrf_token();
            }

            return response()->json( $value );
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
