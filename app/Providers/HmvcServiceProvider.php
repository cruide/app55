<?php namespace App\Providers;

use App\Models\Setting;

class HmvcServiceProvider extends \Illuminate\Support\ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$path = app_path('Http/Modules') . DIRECTORY_SEPARATOR;

		/**
		* Set globals varibles
		*/
		\View::share([
			'base_url'          => url('/'),
			'js_url'            => url('/js'),
			'css_url'           => url('/css'),
			'images_url'        => url('/images'),
		]);

		foreach(@glob("{$path}*", GLOB_ONLYDIR) as $value) {
			$module           = str_replace($path, '', $value);
			$module_path      = $path . $module . DIRECTORY_SEPARATOR;
			$module_namespace = "App\\Http\\Modules\\{$module}";

			/**
			* Add namespace for language
			*/
			if( is_dir($module_path . 'Lang') ) {
				\Lang::addNamespace($module, $module_path . 'Lang');
			}

            /**
             * Add namespace for Blade
             */
			if( is_dir($module_path . 'Views') ) {
				\View::addNamespace($module, $module_path . 'Views');
			}

			/**
			* Add path to module migrations
			*/
			if( is_dir($module_path . 'Migrations') ) {
				$this->loadMigrationsFrom($module_path . 'Migrations');
			}

            /**
             * Add console commands
             */
            if( is_dir($module_path . 'Console') ) {
                if( is_file($module_path . 'Console' . DIRECTORY_SEPARATOR . 'commands.php') ) {
                    require($module_path . 'Console' . DIRECTORY_SEPARATOR . 'commands.php');
                }
            }

			/**
			* Autoload and register module providers
			*/
			if( is_dir($module_path . 'Providers') ) {
				foreach( glob($module_path . 'Providers' . DIRECTORY_SEPARATOR . '*.php') as $value) {
					$provider = str_replace([$module_path . 'Providers' . DIRECTORY_SEPARATOR, '.php'], '', $value);

					if( !class_exists("\\{$module_namespace}\\Providers\\{$provider}") ) {
						require( $value );
					}

					\App::register( "\\{$module_namespace}\\Providers\\{$provider}" );
				}
                
                unset($provider);
			}
            
            /**
            * Autoload and register module policies
            */
            if( is_dir($module_path . 'Policies') ) {
                foreach( glob($module_path . 'Policies' . DIRECTORY_SEPARATOR . '*Policy.php') as $value) {
                    $policy_name = str_replace([$module_path . 'Policies' . DIRECTORY_SEPARATOR, '.php'], '', $value);
                    $model_name  = str_replace([$module_path . 'Policies' . DIRECTORY_SEPARATOR, 'Policy.php'], '', $value);
                    
                    if( !class_exists("\\{$module_namespace}\\Policies\\{$policy_name}") ) {
                        require( $value );
                    }
                    
                    if( is_dir($module_path . 'Models') && is_file($module_path . 'Models' . DIRECTORY_SEPARATOR . "{$model_name}.php") ) {
                        \Gate::policy( "\\{$module_namespace}\\Models\\{$model_name}", "\\{$module_namespace}\\Policies\\{$policy_name}" );
                    }
                }
                
                unset($policy_name, $model_name);
            }

            /*
            if( is_dir($module_path . 'Console') ) {
                if( is_dir($module_path . 'Console' . DIRECTORY_SEPARATOR . 'Commands') ) {

                }
            }
            */
		}
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}
}
