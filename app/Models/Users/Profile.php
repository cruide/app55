<?php namespace App\Models\Users;

class Profile extends \App\Model
{
    const GENDER_MALE   = 1;
    const GENDER_FEMALE = 2;
    const GENDER_OTHER  = 3;
    
    protected $table    = 'user_profiles';
    protected $fillable = [
        'user_id', 'nicname', 'first_name', 'middle_name', 'last_name', 
        'gender', 'birthday', 'stamp', 'avatar_file',
    ];
    
    protected $appends  = [
        'fullname',
        'initials',
        'avatar',
    ];
    
    /**
    * Return URL for avatar image
    */
    public function getAvatarAttribute()
    {
        return url('/uploads/avatars/' . $this->avatar_file);
    }

    /**
    * User full name mutator
    */
    public function getFullnameAttribute()
    {
        $_ = [];
        
        if( !empty($this->attributes['last_name']) ) {
            $_[] = str_ucfirst( 
                str_lower( $this->attributes['last_name'] )
            );
        }
        
        if( !empty($this->attributes['first_name']) ) {
            $_[] = str_ucfirst( 
                str_lower( $this->attributes['first_name'] )
            );
        }

        if( !empty($this->attributes['middle_name']) ) {
            $_[] = str_ucfirst( 
                str_lower( $this->attributes['middle_name'] )
            );
        }
        
        return implode(' ', $_);
    }
    
    /**
    * User initials mutator
    */
    public function getInitialsAttribute()
    {
        $_ = '';
        
        if( !empty($this->attributes['first_name']) ) {
            $_[] = str_upper( 
                mb_substr($this->attributes['first_name'], 0, 1, 'UTF-8')
            ) . '.';
        }
        
        if( !empty($this->attributes['middle_name']) ) {
            $_[] = str_upper( 
                mb_substr($this->attributes['middle_name'], 0, 1, 'UTF-8')
            ) . '.';
        }

        return implode(' ', $_);
    }
    
    /**
    * Return user of it's profile
    */
    public function user()
    {
        return $this->belongsTo( \App\User::class );
    }
}
  

