<?php namespace App\Models\Users;

class Group extends \App\Model
{
    protected $table    = 'user_groups';
    protected $fillable = ['parent_id', 'name', 'description', 'level', 'codename'];

    public function users()
    {
        return $this->hasMany( \App\User::class );
    }
}