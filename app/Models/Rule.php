<?php namespace App\Models;

final class Rule extends \App\Model
{
    protected $table      = 'rules';
    protected $fillable   = ['module', 'controller', 'method', 'level'];

    protected $validation = [
        'module'     => 'required|regex:/[a-z0-9\_]/s|max:128',
        'controller' => 'required|regex:/[a-z0-9\_]/s|max:128',
        'method'     => 'required|regex:/[a-z0-9\_]/s|max:128',
        'level'      => 'numeric',
    ];
}