<?php namespace App\Models\Abstracts;
/**
* Abstract model for Nested Set tree
* @author Alex Tisch
*/

abstract class TreeNS extends \App\Model
{
    public static $tree_cache_childs;
    public static $tree_cache_plain;
    public static $_cache_children_counts;

    public static function boot()
    {
        $class_name = get_called_class();
        $callback   = function($model) {
            $class_name = get_class($model);
            
            if( $model->isDirty('parent_id') ) {
                $parent = $model->parent();
                $class_name::treeRecalc( !empty($parent->id) ? $parent->root_id : $model->id );
            }
        };

        $class_name::saved( $callback );
        $class_name::deleted( $callback );
        
        parent::boot();
    }
    
    /**
    * Conversion of the entire tree or a portion thereof
    * 
    * @param mixed $root_id
    */
    public static function treeRecalc( $root_id = null )
    {
        $class_name = get_called_class();
        $roots      = $class_name::where('parent_id', '=', 0);

        if( !empty($root_id) ) {
            $root = $class_name::find( $root_id );
            
            if( !empty($root->id) ) {
                $roots = $roots->where('id', '=', $root_id);
            }
        }

        $roots = $roots->get();
        
        \DB::beginTransaction();
        
        foreach($roots as $root) {
            $c           = 1;
            $root->lidx  = 1;
            $root->depth = 0;
            
            if( $root->hasChildren(true) ) {
                self::childCaclc($root, $root->id, $c, $root->depth);
            }
            
            $c++;
            $root->root_id = $root->id;
            $root->ridx    = $c;
            
            if( empty($prefix) ) {
                $prefix = $root->getConnection()->getTablePrefix();
                $table  = $root->getTable();
            }
            
            \DB::update("update `{$prefix}{$table}` set `lidx` = ?, `ridx` = ?, `depth` = ?, `root_id` = ? WHERE `id` = ?", [
                $root->lidx, $root->ridx, $root->depth, $root->root_id, $root->id
            ]);
        }
        
        \DB::commit();
    }
    
    /**
    * Conversion of the selected portion of the tree
    * 
    * @param mixed $parent
    * @param mixed $root_id
    * @param mixed $c
    * @param mixed $depth
    */
    public static function childCaclc($parent, $root_id, &$c, $depth)
    {
        $depth++;

        $prefix = $parent->getConnection()->getTablePrefix();
        $table  = $parent->getTable();
        
        foreach($parent->getChildren(true) as $children) {
            $c++;
            $children->lidx    = $c;
            $children->root_id = $root_id;
            $children->depth   = $depth;
            
            if( $children->hasChildren(true) ) {
                self::childCaclc($children, $root_id, $c, $depth);
            }
            
            $c++;
            $children->ridx = $c;

            \DB::update("update `{$prefix}{$table}` set `lidx` = ?, `ridx` = ?, `depth` = ?, `root_id` = ? WHERE `id` = ?", [
                $children->lidx, $children->ridx, $children->depth, $children->root_id, $children->id,
            ]);
        }
    }
    
    /**
    * Get tree root
    * 
    * @param mixed $id
    */
    public static function getRoot($id)
    {
        $class_name = get_called_class();
        $item       = $class_name::find( $id );

        if( empty($item) ) {
            return false;
        }

        return $item->root();
    }

    /**
     * Функция возвращает набор объектов отсортированный по уровню вложенности (+добавляется свойство $depth - глубина в иерархии)
     *
     * @param integer $parent_id - id родителя от которого необходимо строить  дерево
     * @param bool $include_root - включать или не включать родительский элемент в выборку
     * @param array $enabled_ids - Разрешенные для построения дерева id узлов
     *
     * @return array
     */
    public static function getItemsPlainList($parent_id = 0, $include_root = false)
    {
        $class_name = get_called_class();
        $res        = [];

        if( !is_array(self::$tree_cache_plain) ) {
            self::$tree_cache_plain = [];
        }

        if( empty($parent_id) ) {
            $childs = $class_name::get();
            
        } else {
            $parent = $class_name::find( $parent_id );
            
            if( !array_key_exists($class_name, self::$tree_cache_plain) ) {
                self::$tree_cache_plain[ $class_name ] = [];
            }
            
            if( !empty($parent) ) {
                if( !empty(self::$tree_cache_plain[ $class_name ][ $parent->root_id ]) ) {
                    $childs = self::$tree_cache_plain[ $class_name ][ $parent->root_id ];
                    
                } else {

                    $childs = $class_name::where('root_id', '=', $parent->root_id)
                                         ->orderBy('lidx', 'ASC')
                                         ->orderBy('position', 'ASC')
                                         ->get();
                                  
                    self::$tree_cache_plain[ $class_name ][ $parent->root_id ] = $childs;
                }
                              
            } else {
                return [];
            }
        }

        if( $include_root == true ) {
            foreach($childs as $key=>$val) {
                if( $val->lidx >= $parent->lidx && $val->ridx <= $parent->ridx ) {
                    $res[] = $val;
                }
            }
        } else {
            foreach($childs as $key=>$val) {
                if( $val->lidx >= $parent->lidx && $val->ridx <= $parent->ridx && $val->id != $parent->id ) {
                    $res[] = $val;
                }
            }
        }

        return $res;
    }

    /**
     * Функция возвращает массив родительских объектов
     *
     * @param integer $parent_id - parent_id бытийности для которой возвращается выборка
     *
     * @return  array
     */

    public static function getParentsIdsArray($parent_id, &$parents)
    {
        $class_name = get_called_class();
        $obj        = $class_name::find( $parent_id );

        if( $obj ) {
            if( $obj->parent_id > 0 ) { //'lidx', [$obj->lidx, $obj->ridx]
                $items = self::getParents( $parent_id );
                
                foreach($items as $item) {
                    $parents[] = $item->id;
                }
            }
            
            if( !in_array($obj->id, $parents) ) {
                $parents[] = $obj->id;
            }
        }
    }

    /**
     * Функция для построения  fancytree дерева от $id  родителя
     * Без использования кеширования
     *
     * @param integer $id - id родителя
     * @param array $_tree
     * @param Callable $callback - callback для назначения атрибутов
     * @return  array
     */

    public static function makeTree($id, &$_tree, Callable $callback = null)
    {
        $class_name = get_called_class();
        $parent     = $class_name::find( $id );
        
        if( empty($parent) ) {
            return false;
        }

        if( !is_array(self::$tree_cache_plain) ) {
            self::$tree_cache_plain = [];
        }
        
        if( !array_key_exists($class_name, self::$tree_cache_plain) ) {
            self::$tree_cache_plain[ $class_name ] = [];
        }
        
        $data = ($callback == null) ? $parent->getAttributes() : $callback( $parent );
        
        if( !empty(self::$tree_cache_plain[ $class_name ][ $parent->root_id ]) ) {
            $childs = self::$tree_cache_plain[ $class_name ][ $parent->root_id ];
            
        } else {
            
            $childs = $class_name::where('root_id', '=', $parent->root_id)
                                 ->orderBy('lidx', 'ASC')
                                 ->orderBy('position', 'ASC')
                                 ->get();
                          
            self::$tree_cache_plain[ $class_name ][ $id ] = $childs;
        }

        $_ = [];

        foreach($childs as $children) {
            if( $parent->lidx < $children->lidx && $parent->ridx > $children->ridx ) {
                $_[] = $children;
            }
        }
        
        unset($childs, $children);

        if( count($_) > 0 ) {
            $data['children'] = self::makeChildren($parent, $callback, $_);
            $data['expanded'] = true;
        }

        $_tree[] = $data;
    }
    
    public static function isChildren($parent_id, $items)
    {
        foreach($items as $item) {
            if( $parent_id == $item->parent_id ) {
                return true;
            }
        }
        
        return false;
    }
    
    public static function makeChildren($item, $build, $items) 
    {
        if( empty($item->id) ) {
            return false;
        }

        if( empty($item->key) ) {
            $item->key = $item->id;
        }
        
        $_ = [];
        
        foreach($items as $child) {
            if( $child->parent_id == $item->id ) {
                $data = ($build == null) ? $child->getAttributes() : $build( $child );

                if( empty($data) ) continue;
                
                if( self::isChildren($child->id, $items) ) {
                    $data['children'] = self::makeChildren($child, $build, $items);
                }
                
                $_[] = $data;
            }
        }
        
        return $_;
    }
    
    /**
     * Функция для поиска элемента в  fancytree дереве по  параметру
     *
     * @param array $_tree - дереао
     * @param string $name - имя параметра
     * @param string $value - значение
     * @return  array
     */

    public static function findTreeElement($_tree, $name, $value)
    {
        if(is_array($_tree)) {
            foreach($_tree as $element) {
                if(isset($element[$name]) && $element[$name] == $value) {
                    return $element;
                }
                if(isset($element['children'])) {
                    $child = self::findTreeElement($element['children'], $name, $value);
                    if(isset($child[$name]) && $child[$name] == $value) {
                        return $child;
                    }
                }
            }
        }

        return null;

    }

    /**
     * Функция возвращает набор родительских объектов
     *
     * @param integer $parent_id - parent_id бытийности для которой возвращается выборка
     *
     * @return  array
     */

    public static function getParents($parent_id)
    {
        $class_name = get_called_class();
        $p          = $class_name::find( $parent_id );

        if( !is_array(self::$tree_cache_plain) ) {
            self::$tree_cache_plain = [];
        }

        if( $p ) {
            
            if( !array_key_exists($class_name, self::$tree_cache_plain) ) {
                self::$tree_cache_plain[ $class_name ] = [];
            }
            
            if( $p->parent_id == 0 ) {
                return [ $p ];

            } else {
                $root = $p->root();
                
                if( !empty(self::$tree_cache_plain[ $class_name ][ $root->id ]) ) {
                    $items = self::$tree_cache_plain[ $class_name ][ $root->id ];
                    
                } else {
                    
                    $items = $class_name::where('root_id', '=', $root->id)
                                        ->orderBy('lidx', 'ASC')
                                        ->orderBy('position', 'ASC')
                                        ->get();

                    self::$tree_cache_plain[ $class_name ][ $root->id ] = $items;
                }
                
                $_ = [];

                foreach($items as $item) {
                    if( $item->lidx < $p->lidx && $item->ridx > $p->ridx ) {
                        $_[] = $item;
                    }
                }

                return $_;
            }
        }

        return [];
    }

    /**
     * Функция применяет функцию к каждому из цепочки парентов
     *
     * @param int $parent_id
     * @param callable $callback
     * @return  array
     */

    public static function parents($parent_id, $callback)
    {
        $class_name = get_called_class();
        $parent     = $class_name::find( $parent_id );
        
        if( $parent ) {
            $return = $callback( $parent );
            
            if( $return !== null ) {
                return $return;
                
            } else {
                if( $parent->parent_id > 0 ) {
                    return self::parents($parent->parent_id, $callback);
                }
            }
        }

        return null;
    }

    public function hasChildren(  $cache_refrash = false )
    {
        $class_name = get_class( $this );

        if( !is_array(self::$_cache_children_counts) ) {
            self::$_cache_children_counts = [];
        }

        if( empty($this->id) ) {
            return false;
        }
        
        if( !array_key_exists($class_name, self::$_cache_children_counts) ) {
            self::$_cache_children_counts[ $class_name ] = [];
        }
        
        
        if( array_key_exists($this->id, self::$_cache_children_counts[ $class_name ]) && !$cache_refrash ) {
            if( self::$_cache_children_counts[ $class_name ][ $this->id ] > 0 ) {
                return true;
            }
            
            return false;
        }
        
        self::$_cache_children_counts[ $class_name ][ $this->id ] = $class_name::where('parent_id', '=', $this->id)->count();
        
        if( self::$_cache_children_counts[ $class_name ][ $this->id ] > 0 ) {
            return true;
        }
        
        return false;
    }
    
    public function getChildren( $cache_refrash = false )
    {
        $class_name = get_class( $this );

        if( empty(self::$tree_cache_childs) ) {
            self::$tree_cache_childs = [];
        }
        
        if( empty(self::$tree_cache_childs[ $class_name ]) ) {
            self::$tree_cache_childs[ $class_name ] = [];
        }

        if( !empty(self::$tree_cache_childs[ $class_name ][ $this->id ]) && !$cache_refrash ) {
            return self::$tree_cache_childs[ $class_name ][ $this->id ];
        }

        $builder = $class_name::where('parent_id', '=', (int)$this->id);

        if( \Schema::hasColumn($this->getTable(), 'position') ) {
            $builder->orderBy('position', 'ASC');
        }

        return self::$tree_cache_childs[ $class_name ][ $this->id ] = $builder->get();
    }
    
    public function move($new_parent_id)
    {
        $old_parent_id   = $this->parent_id;
        $this->parent_id = intval($new_parent_id);
        
        if( $this->save() ) {
            self::treeRecalc( $this->root()->id );
            
            if( $old_parent_id != $this->parent_id ) {
                self::treeRecalc( $old_parent_id );
            }
        }

        return true;
    }
    /*
    public function deleteBranch()
    {
        $class_name = get_class( $this );
        $plain_list = self::getPlain($this->id);
        $parent_id  = $this->parent_id;

        foreach($plain_list as $id=>$item) {
            $res     = $class_name::find( $parent_id );
            $root_id = $res->root()->id;
            
            $p->delete();
            
            self::treeRecalc( $root_id );
        }

        $this->delete();
        self::treeRecalc( $parent_id );
    }
    */
    public static function getPlain($parent_id = 0)
    {
        return self::getLevel($parent_id);
    }

    private static function getLevel($parent_id, $depth = 0)
    {
        $list = self::where('parent_id', '=', $parent_id)->get();
        $tree = [];

        foreach($list as $item) {
            $tree[ $item->id ] = ($depth > 0 ? ' |' . str_repeat('--', $depth) . ' ' : '') . $item->name;

            if( $item->hasChildren() ) {
                $tree += self::getLevel($item->id, ($depth + 1));
            }
        }

        return $tree;
    }

    /**
     * Метод возвращает корневой элемент ветки для данного узла
     *
     */
    public function root()
    {
        $class_name = get_called_class();

        if( !empty($this->root_id) ) {
            return $class_name::find( $this->root_id );
            
        } else if( !empty($this->parent_id) ) {
            $char = &$this;
            $i    = 0;

            while( $char && $char->parent_id > 0 || $i < 200 ) {
                $parent = $char->parent();

                if( !$parent ) {
                    return $char;
                } else {
                    $char = $parent;
                }

                $i++;
            }
            
            return $char;
        }
        
        return $this;
    }

    public function parent()
    {
        $class_name = get_called_class();

        return $class_name::find( $this->parent_id );
    }

    /**
     *
     * Метод мутатор для пересчета позиции на одном уровне ветки
     * @param mixed $value
     * */

    public function setPositionAttribute($value)
    {
        $position = 0;

        if ($value < 0) $value = 0;
        
        $_minus = self::where('parent_id', '=', $this->parent_id)->where('position', '<', 0)->count();
        $_null  = self::where('parent_id', '=', $this->parent_id)->where('position', '=', 0)->count();
        
        if( $_minus > 0 || $_null > 1 ) {
            foreach(self::where('parent_id', '=', $this->parent_id)->orderBy('position', 'ASC')->get() as $level_element) {
                if ($level_element->id != $this->id) {
                    $level_element->attributes['position'] = $position;
                    $level_element->save();

                    $position++;
                }
            }
        }

        if( $value === 'end' ) {
            $value = self::where('parent_id', '=', $this->parent_id)->max('position') + 1;
            
        } else {

            $prev_value = 0;
            
            $items = self::where('parent_id', '=', $this->parent_id)
                         ->orderBy('position', 'ASC')
                         ->get();

            foreach($items as $level_element) {

                if( $level_element->position >= $value && $level_element->id != $this->id ) {
                    $level_element->attributes['position'] += 1;
                    $level_element->save();

                    $prev_value = $level_element->attributes['position'];
                }
            }
        }

        $this->attributes['position'] = $value;
    }
    
    public function getAllPartyIds()
    {
        $items = self::getItemsPlainList( $this->root()->id, true );
        $_     = [];
        
        foreach($items as $item) {
            $_[] = $item->id;
        }
        
        return $_;
    }
}