<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Users\Profile;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'group_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public static function boot()
    {
        self::created( function($obj) {
            Profile::create([
                'user_id'  => $obj->id,
                'birthday' => date('Y-m-d'),
            ]);
        });
        
        self::deleting( function($obj) {
            if( $obj->profile ) {
                if( \Storage::disk('avatars')->exists($obj->profile->avatar_file) ) {
                    \Storage::disk('avatars')->delete( $obj->profile->avatar_file );
                }
                
                $obj->profile->delete();
            }
        });
    }

    /**
    * Get user profile
    */
    public function profile()
    {
        return $this->hasOne( \App\Models\Users\Profile::class );
    }
    
    public function group()
    {
        return $this->belongsTo( \App\Models\Users\Group::class );
    }
    
    public function hasLevel( $level )
    {
        return ($this->group->level >= $level) ? true : false;
    }
    
    /**
    * Checking for administrative account
    */
    public function isAdmin()
    {
        return $this->hasLevel(80);
    }
    
    /**
    * Checking for Superuser account
    */
    public function isSuperUser()
    {
        return $this->hasLevel(99);
    }
}
