<?php namespace App\Http\Modules\Feedback\Models;

use App\Http\Modules\Feedback\Mail\FeedbackMail;

class Feedback extends \App\Model
{
    const STATUS_NEW    = 1;
    const STATUS_UNREAD = 2;
    const STATUS_READ   = 3;
    
    protected $fillable = [
        'email', 'name', 'subject', 'body', 'status', 
    ];
    
    protected $validation = [
        'email'   => 'required|email',
        'name'    => 'required|string|max:255',
        'subject' => 'required|string|max:255',
        'body'    => 'required|string',
    ];
    
    public static function boot()
    {
        self::created( function( $obj ) {
            \Mail::to( env('MAIL_FEEDBACK') )
                 ->queue( new FeedbackMail($obj) );
        });
    }
    
    public function scopeNew( $query )
    {
        return $query->whereStatus( self::STATUS_NEW );
    }
}