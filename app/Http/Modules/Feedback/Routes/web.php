<?php

Route::group(['prefix' => 'feedback', 'namespace' => 'Feedback'], function($router) {
    $router->post('send', ['as' => 'feedback.send', 'uses' => 'Index@postSend']);
    $router->get('/'    , ['as' => 'feedback'     , 'uses' => 'Index@getIndex']);
});