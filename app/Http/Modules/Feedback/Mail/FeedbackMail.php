<?php namespace App\Http\Modules\Feedback\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Modules\Feedback\Models\Feedback;

class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $_model;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Feedback $model )
    {
        $this->_model = $model;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Feedback::mails.feedback', ['model' => $this->_model])
                    ->from( $this->_model->email, $this->_model->name )
                    ->subject( $this->_model->subject);
    }
}