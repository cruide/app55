<?php namespace App\Http\Modules\Feedback;

use \App\Http\Modules\Feedback\Models\Feedback;

class Index extends \App\Http\Modules\Controller
{
    public function getIndex()
    {
        return view('Feedback::feedback.index');
    }
    
    public function postSend()
    {
        if( !($form = request('form')) ) {
            abort(404);
        }
        
        $feedback = new Feedback( $form );
        
        if( !$feedback->isValid() ) {
            return redirect()->back()
                             ->withInput( request()->all() )
                             ->withErrors( $feedback->getErrors() );
        }
        
        $feedback->save();
        
//        \Mail::
        
        return redirect()->back()
                         ->withMessage([
                            'message' => 'Your feedback message sent!',
                         ]);
    }
}

