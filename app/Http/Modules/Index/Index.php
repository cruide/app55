<?php namespace App\Http\Modules\Index;
/**
* Index controller
* 
* @author Alex Tisch
*/

use App\Http\Modules\Pages\Models\Page;

class Index extends \App\Http\Modules\Controller
{
    public function getIndex()
    {
        return view('Index::default', [
            'page' => Page::published()->default()->first(),
        ]);
    }
}

