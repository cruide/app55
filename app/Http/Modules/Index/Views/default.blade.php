@extends('layout')
@section('content')

@if( !empty($page) )
<div class="page-header col-md-12">
    <h3 class="pull-left">{{ $page->title }}</h3>
</div>

<div class="clearfix"></div>

<hr/>

<div class="row" style="padding-top: 20px;">
    {!! $page->content !!}
</div>
@else

<div class="jumbotron">
    <h1>HMVC Application example</h1>
    <p>This is a template showcasing the optional theme stylesheet included in Bootstrap. Use it as a starting point to create something more unique by building on or modifying it.</p>
</div>

@endif

@endsection
