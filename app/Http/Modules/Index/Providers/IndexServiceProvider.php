<?php namespace App\Http\Modules\Index\Providers;

use Illuminate\Support\ServiceProvider;

class IndexServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
