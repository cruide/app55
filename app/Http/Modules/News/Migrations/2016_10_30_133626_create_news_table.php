<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Rule;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('uri')->index()->default('');
            $table->string('title')->default('');
            $table->string('keywords')->default('');
            $table->text('preview');
            $table->text('content');
            $table->tinyInteger('published')->index()->default(0);
            $table->bigInteger('user_id')->index()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'getList'  , 'level' => 50]);
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'getEdit'  , 'level' => 50]);
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'postEdit' , 'level' => 50]);
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'getAdd'   , 'level' => 50]);
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'postAdd'  , 'level' => 50]);
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'getDelete', 'level' => 50]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
