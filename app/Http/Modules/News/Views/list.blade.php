@extends('layout')
@section('content')

<div class="page-header col-md-12">
    <h3 class="pull-left"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Управление новостями</h3>
    <a href="{{ url('/news/create') }}" class="btn  btn-sm btn-default pull-right" style="margin-top: 20px;">
        <span class="glyphicon glyphicon-plus"></span>
        &nbsp;
        Добавить новость
    </a>
</div>

<div class="clearfix"></div>

<div class="col-md-12" style="padding-top: 20px;">
    <table class="table table-striped" role="table" id="app-admin-news-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>@ufl('strings.title')</th>
                <th>URI</th>
                <th>Дата создания</th>
                <th>&nbsp;</th>
            </tr>        
        </thead>
        <tbody>
            @foreach($items as $item)
            <tr class="app-admin-news-{{ $item->id }}">
                <td>{{ $item->id }}</td>
                <td>{{ $item->title }}</td>
                <td>{{ $item->uri }}</td>
                <td>{{ $item->created_at->format('d.m.Y') }}</td>
                <td>
                    <a href="{{ url('/news/edit/' . $item->id) }}" style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span></a>
                    &nbsp;
                    <a onclick="appBackNewsDelete('{{ $item->id }}')" style="cursor: pointer;"><span class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@push('styles')
<link rel="stylesheet" href="{{ $js_url }}/datatables/datatables.css" />
@endpush

@push('scripts')
<script src="{{ $js_url }}/datatables/datatables.min.js"></script>
<script type="text/javascript">
var appBackNewsDataTable = null;
var appBackNewsDelete    = function( id ) {
    if( !_app.empty(id) ) {
        _app.confirm('Вы действительно хотите удалить страницу?', 'Удаление страницы', function() {
            _app.jsonGet("{{ url('/news/delete') }}/" + id, function(r) {
                if( !_app.empty(r.success) && r.success == true ) {
                    appBackNewsDataTable.row( $('.app-admin-news-' + id).closest('tr').get(0) ).remove().draw();
                }
            });
        });
    }
};

$(function() {
    appBackNewsDataTable = $('#app-admin-news-table').DataTable({
        paging: true,
        lengthChange: false,
        searching: true,
        ordering: true,
        info: true,
        autoWidth: false,
    });
});
</script>
@endpush