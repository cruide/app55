@extends('layout')
@section('content')
<div class="row">
    @foreach($items as $item)
    <div class="col-md-4 col-sm-12">
        <h3>{{ $item->title }}</h3>
        
        <div class="col-md-12">
            {!! $item->preview !!}
        </div>
        
        <div class="col-md-12">
            <strong>@ufl('strings.date'):</strong> {{ $item->created_at->format('d.m.Y') }} | <strong>@ufl('strings.author'):</strong> {{ $item->author->name }}
        </div>
    </div>
    @endforeach

    <div class="col-md-12">
        {!! $items->render() !!}
    </div>
    
</div>
@endsection