<?php namespace App\Http\Modules\News;

use App\Http\Modules\News\Models\News;

class Index extends \App\Http\Modules\Controller
{
    /**
    * Full listing for news
    */
    public function getIndex()
    {
        return view('News::index', [
            'items' => News::published()->paginate(25),
        ]);
    }
    
    /**
    * Show full news by URI
    * 
    * @param string $uri
    * @return {\Illuminate\Contracts\View\Factory|\Illuminate\View\View}
    */
    public function getFull( $uri = null )
    {
        if( empty($uri) || !($item = News::byUri($uri)) ) {
            abort(404);
        }
        
        return view('News::full', [
            'item' => $item,
        ]);
    }
    
    /**
    * Show list of news for admin
    */
    public function getList()
    {
        return view('News::list', [
            'items' => News::all(),
        ]);
    }
    
    /**
    * Add new news item
    */
    public function getAdd()
    {
        return $this->getEdit();
    }
    
    /**
    * Add new news item
    * Save method
    */
    public function postAdd()
    {
        return $this->postEdit();
    }
    
    /**
    * Edit news by ID
    * 
    * @param integer $id
    * @return {\Illuminate\Contracts\View\Factory|\Illuminate\View\View}
    */
    public function getEdit( $id = null )
    {
        if( empty($id) || !($item = News::find($id)) ) {
            $item = new News();
        }
        
        return view('News::edit', [
            'item'      => $item,
            'old_input' => old('form'),
        ]);
    }
    
    /**
    * Save edited news by ID
    * 
    * @param integer $id
    * @return {\Illuminate\Http\RedirectResponse|\Illuminate\Http\RedirectResponse}
    */
    public function postEdit( $id = null )
    {
        if( empty($id) || !($item = News::find($id)) ) {
            $item = new News();
        }

        $form      = array_trim( request()->get('form') );
        $validator = $item->validator( $form );
        
        
        if( $validator->fails() ) {
            return redirect()->back()
                             ->withInput( request()->all() )
                             ->withErrors( $validator->errors() );
        }

        $item->fill( $form );
        
        if( empty($item->user_id) ) {
            $item->user_id = auth()->id();
        }
        
        $item->save();
        
        return redirect('/news/edit/' . $item->id)
                   ->with('message', 'Страница успешно сохранена!');
    }
    
    /**
    * Delete news by ID
    * 
    * @param integer $id
    * @return \Illuminate\Http\JsonResponse
    */
    public function getDelete( $id = null )
    {
        if( empty($id) || !is_numeric($id) || !($item = News::find($id)) ) {
            return response()->json([
                'success' => false,
                'message' => 'Incorrect page identifier',
            ]);
        }
        
        $item->delete();
        
        return response()->json([
            'success' => true,
            'id'      => $id,
        ]);
    }
}

