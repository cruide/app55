<?php namespace App\Http\Modules\News\Models;

class News extends \App\Model
{
    protected $fillable   = [
        'uri', 'user_id', 'title', 'keywords', 
        'published', 'content', 'preview',
    ];
    
    protected $casts      = [
        'published' => 'boolean',
    ];
    
    protected $validation = [
        'title'     => 'required|string|max:255',
        'uri'       => 'required|regex:/[a-z0-9\-]/s|max:255',
        'preview'   => 'string',
        'content'   => 'string',
        'published' => 'numeric',
    ];
    
    public static function byUri( $uri = null )
    {
        return !empty($uri) ? self::whereUri($uri)->published()->first() : null;
    }

    public function scopePublished( $query )
    {
        return $query->wherePublished(1);
    }
    
    public function author()
    {
        return $this->belongsTo( \App\User::class, 'user_id' );
    }
}