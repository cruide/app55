<?php

Route::group(['prefix' => 'news', 'namespace' => 'News'], function($router) {
    $router->get('list'          , ['as' => 'news.list'       , 'uses' => 'Index@getList']);
    $router->get('full/{uri}'    , ['as' => 'news.full'       , 'uses' => 'Index@getFull']);
    $router->get('create'        , ['as' => 'news.create'     , 'uses' => 'Index@getAdd']);
    $router->post('create'       , ['as' => 'news.create.save', 'uses' => 'Index@postAdd']);
    $router->get('edit/{id}'     , ['as' => 'news.edit'       , 'uses' => 'Index@getEdit']);
    $router->post('edit/{id}'    , ['as' => 'news.edit.save'  , 'uses' => 'Index@postEdit']);
    $router->delete('delete/{id}', ['as' => 'news.delete'     , 'uses' => 'Index@getDelete']);
    $router->get('/'             , ['as' => 'news'            , 'uses' => 'Index@getIndex']);
});
