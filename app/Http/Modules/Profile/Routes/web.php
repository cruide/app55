<?php

Route::get('profile', [
    'as'   => 'profile.get',
    'uses' => 'Profile\\Index@getIndex',
])->middleware('auth');

Route::post('profile', [
    'as'   => 'profile.post',
    'uses' => 'Profile\\Index@postIndex',
])->middleware('auth');

