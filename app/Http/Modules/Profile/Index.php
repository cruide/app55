<?php namespace App\Http\Modules\Profile;

class Index extends \App\Http\Modules\Controller
{
    public function getIndex()
    {
        return view('Profile::index', [
            'user' => auth()->user(),
        ]);
    }
    
    public function postIndex()
    {
        $form = request('form');
        $user = auth()->user();
        
        $this->validate(request(), [
            'form.gender'      => 'required|numeric',
            'form.name'        => 'required|string|max:32',
            'form.first_name'  => 'string|max:255',
            'form.middle_name' => 'string|max:255',
            'form.last_name'   => 'string|max:255',
            'form.birthday'    => 'date',
            'form.passwd1'     => 'alpha_num|max:16',
            'form.passwd2'     => 'alpha_num|max:16',
        ]);
        
        $user->name = $form['name'];
        
        $user->profile->gender      = $form['gender'];
        $user->profile->first_name  = $form['first_name'];
        $user->profile->middle_name = $form['middle_name'];
        $user->profile->last_name   = $form['last_name'];
        $user->profile->birthday    = carbon($form['birthday'])->format('Y-m-d');
        
        if( !empty($form['passwd1']) && !empty($form['passwd2']) 
         && $form['passwd1'] == $form['passwd2']
         && $user->password != $form['passwd1'] )
        {
            $user->password != $form['passwd1'];
        }
        
        $user->push();
        
        return redirect('profile')->with('message', 'Данные успешно сохранены');
    }

}

