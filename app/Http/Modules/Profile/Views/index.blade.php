@extends('layout')
@section('content')
<div class="page-header">
  <h3><span class="glyphicon glyphicon-user"></span> @ufl('Profile::profile.user_profile')</h3>
</div>

@if( session()->has('message') )
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Отлично!</strong> {{ session('message') }}
</div>
@endif

@if( $errors->count() > 0 )
<div class="alert alert-warning alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong>Внимание!</strong> При заполнении формы были допущены ошибки.<br />
</div>
@endif                        

<div class="col-xs-10">
    <form role="form" class="form-horizontal col-xs-10" action="{{ url('/profile') }}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        
        <div class="form-group @if( $errors->has('form.name') ) has-error @endif">
            <label for="user-nicname" class="control-label col-xs-4">@ufl('strings.name'):</label>

            <div class="col-xs-8">
                <input type="text" name="form[name]" class="form-control" id="user-nicname" value="@if( old('name') != '' ){{ old('name') }}@else{{ $user->name }}@endif" maxlength="32" />
            </div>
        </div>

        <div class="form-group @if( $errors->has('form.first_name') ) has-error @endif">
            <label for="user-first-name" class="control-label col-xs-4">
                @ufl('Profile::profile.first_name'):
            </label>

            <div class="col-xs-8">
                <input type="text" name="form[first_name]" class="form-control" id="user-first-name" value="@if( old('first_name') != '' ){{ old('first_name') }}@else{{ $user->first_name }}@endif" maxlength="32" />
            </div>
        </div>

        <div class="form-group @if( $errors->has('form.middle_name') ) has-error @endif">
            <label for="user-middle-name" class="control-label col-xs-4">@ufl('Profile::profile.middle_name'):</label>

            <div class="col-xs-8">
                <input type="text" name="form[middle_name]" class="form-control" id="user-middle-name" value="@if( old('middle_name') != '' ){{ old('middle_name') }}@else{{ $user->middle_name }}@endif" maxlength="32" />
            </div>
        </div>

        <div class="form-group @if( $errors->has('form.last_name') ) has-error @endif">
            <label for="user-last-name" class="control-label col-xs-4">@ufl('Profile::profile.last_name'):</label>

            <div class="col-xs-8">
                <input type="text" name="form[last_name]" class="form-control" id="user-last-name" value="@if( old('last_name') != '' ){{ old('last_name') }}@else{{ $user->profile->last_name }}@endif" maxlength="32" />
            </div>
        </div>

        <div class="form-group @if( $errors->has('form.birthday') ) has-error @endif">
            <label for="user-birthday" class="control-label col-xs-4">@ufl('strings.birthday')</label>

            <div class="col-xs-8">
                <input type="text" 
                       name="form[birthday]" 
                       id="user-birthday" 
                       class="form-control"
                       data-date="{{ carbon($user->profile->birthday)->format('d.m.Y') }}" 
                       data-date-format="dd.mm.yyyy"
                       value="{{ carbon($user->profile->birthday)->format('d.m.Y') }}" 
                       maxlength="32"
                       readonly="readonly" />
            </div>
        </div>
        
        <div class="form-group @if( $errors->has('form.gender') ) has-error @endif">
            <label for="user-gender" class="control-label col-xs-4">@ufl('strings.gender'):</label>

            <div class="col-xs-8">
                <select name="form[gender]" class="form-control" id="user-gender">
                    <option value="1" @if( $user->profile->gender == \App\Models\Users\Profile::GENDER_MALE ) selected="selected" @endif >@ufl('strings.male')</option>
                    <option value="2" @if( $user->profile->gender == \App\Models\Users\Profile::GENDER_FEMALE ) selected="selected" @endif >@ufl('strings.female')</option>
                    <option value="3" @if( $user->profile->gender == \App\Models\Users\Profile::GENDER_OTHER ) selected="selected" @endif >@ufl('strings.other')</option>
                </select>
            </div>
        </div>

        <div class="form-group @if( $errors->has('form.passwd1') ) has-error @endif">
            <label for="user-passwd1" class="control-label col-xs-4">@ufl('strings.password'):</label>

            <div class="col-xs-8">
                <input type="password" name="form[passwd1]" class="form-control" id="user-passwd1" maxlength="16" />
            </div>
        </div>

        <div class="form-group @if( $errors->has('form.passwd2') ) has-error @endif">
            <label for="user-passwd2" class="control-label col-xs-4">Повторить пароль:</label>

            <div class="col-xs-8">
                <input type="password" name="form[passwd2]" class="form-control" id="user-passwd2" maxlength="16" />
            </div>
        </div>

        <div class="col-xs-10"></div>
        <div class="col-xs-2">
            <button type="submit" class="btn btn-primary">
                <span class="glyphicon glyphicon-floppy-save"></span>&nbsp;
                @ufl('strings.save')
            </button>
        </div>
        
    </form>
</div>
@endsection

@push('scripts')
<script type="text/javascript" src="{{ $js_url }}/bootstrap-datepicker.js"></script>
<script type="text/javascript">
$(function(){
    $('#user-birthday').datepicker();
});
</script>
@endpush