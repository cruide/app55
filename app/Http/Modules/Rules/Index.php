<?php namespace App\Http\Modules\Rules;

use App\Models\Rule;

class Index extends \App\Http\Modules\Controller
{
    public function getIndex()
    {
        $access = [];
        
        foreach(\App\Models\Users\Group::all() as $group) {
            $access[ $group->level ] = $group->name;
        }
        
        return view('Rules::index', [
            'rules'  => Rule::all(),
            'access' => $access,
        ]);
    }
    
    public function getAdd()
    {
        return $this->getEdit();
    }
    
    public function postAdd()
    {
        return $this->postEdit();
    }
    
    public function getEdit( $id = null )
    {
        if( empty($id) || !($rule = Rule::find($id)) ) {
            $rule = new Rule();
        }
        
        return view('Rules::edit', [
            'rule'    => $rule,
            'modules' => get_modules(),
        ]);
    }
    
    public function postEdit( $id = null )
    {
        if( empty($id) || !($rule = Rule::find($id)) ) {
            $rule = new Rule();
        }
                   
        $form      = array_trim( request()->get('form') );
        $validator = $rule->validator($form);
        
        if( $validator->fails() ) {
            return redirect()->back()
                             ->withInput( request()->all() )
                             ->withErrors( $validator->errors() );
        }

        $rule->fill( $form );
        $rule->user_id = auth()->id();
        $rule->save();
        
        return redirect('/rules/edit/' . $page->id)
                   ->with('message', 'Правило успешно создано!');
    }
    
    /**
    * Delete rule
    * 
    * @param mixed $id
    * @return \Illuminate\Http\JsonResponse
    */
    public function getDelete( $id = null )
    {
        if( empty($id) || !($rule = Rule::find($id)) ) {
            return response()->json([
                'success' => false,
                'error'   => 'Incorrect rule indentifier',
            ]);
        }
        
        $rule->delete();
        
        return response()->json([
            'success' => true,
            'id'      => $id,
        ]);
    }
}

