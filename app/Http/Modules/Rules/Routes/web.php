<?php

Route::group(['prefix' => 'rules', 'namespace' => 'Rules'], function() {
    Route::get('create'     , ['as' => 'rules.create'     , 'uses' => 'Index@getAdd']);
    Route::post('create'    , ['as' => 'rules.create.save', 'uses' => 'Index@postAdd']);
    Route::get('edit/{id}'  , ['as' => 'rules.edit'       , 'uses' => 'Index@getEdit']);
    Route::post('edit/{id}' , ['as' => 'rules.edit.save'  , 'uses' => 'Index@postEdit']);
    Route::get('delete/{id}', ['as' => 'rules.delete'     , 'uses' => 'Index@getDelete']);
    Route::get('/'          , ['as' => 'rules'            , 'uses' => 'Index@getIndex']);
});

