@extends('layout')
@section('content')

<div class="page-header col-md-12">
    <h3 class="pull-left"><span class="glyphicon glyphicon-lock"></span> Управление правилами доступа</h3>
    <a href="{{ url('/rules/create') }}" class="btn btn-sm btn-default pull-right" style="margin-top: 20px;">
        <span class="glyphicon glyphicon-plus"></span>
        &nbsp;
        Добавить правило
    </a>
</div>

<div class="clearfix"></div>

<div class="col-md-12" style="padding-top: 20px;">
    <table class="table table-striped" role="table" id="app-admin-rules-table">
        <thead>
            <tr>
                <th>@ufl('strings.module')</th>
                <th>@ufl('strings.controller')</th>
                <th>@ufl('strings.method')</th>
                <th>@ufl('strings.access')</th>
                <th>&nbsp;</th>
            </tr>        
        </thead>
        <tbody>
            @foreach($rules as $rule)
            <tr class="app-admin-rule-{{ $rule->id }}">
                <td>{{ $rule->module }}</td>
                <td>{{ $rule->controller }}</td>
                <td>{{ $rule->method }}</td>
                <td>{{ array_key_isset($rule->level, $access) ? $access[ $rule->level ] : ('>= ' . $rule->level) }}</td>
                <td>
                    <a href="{{ url('/rules/edit/' . $rule->id) }}" style="cursor: pointer;">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                    &nbsp;
                    <a onclick="appBackAccessDelete('{{ $rule->id }}')" style="cursor: pointer;">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@push('styles')
<link rel="stylesheet" href="{{ $js_url }}/datatables/datatables.css" />
@endpush

@push('scripts')
<script src="{{ $js_url }}/datatables/datatables.min.js"></script>
<script type="text/javascript">
var appBackAccessDataTable = null;
var appBackAccessDelete    = function( id ) {
    if( !_app.empty(id) ) {
        _app.confirm('Вы действительно хотите удалить данное правило?', 'Удаление правила', function() {
            _app.jsonGet("{{ url('/rules/delete') }}/" + id, function(r) {
                if( !_app.empty(r.success) && r.success == true ) {
                    appBackAccessDataTable.row( $('.app-admin-rule-' + id).closest('tr').get(0) ).remove().draw();
                }
            });
        });
    }
};

$(function() {
    appBackAccessDataTable = $('#app-admin-rules-table').DataTable({
        paging: true,
        lengthChange: false,
        searching: true,
        ordering: true,
        info: true,
        autoWidth: false,
    });
});
</script>
@endpush