@extends('layout')
@section('content')

<div class="page-header col-md-12">
    <h2>
        <?php echo ($rule->id) ? 'Редактирование правила' : 'Создание правила'; ?>
    </h2>
</div>

<div class="clearfix"></div>

<hr/>

<div class="row" style="padding-top: 20px;">
    <div class="col-md-12">
        <form method="POST" id="rules-edit-form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            
            <div class="col-md-6">
                <div class="form-group">
                    <label>@ufl('strings.module')</label>

                    <select id="rules-edit-form-module" class="form-control" name="form[module]" onchange="appAccessSetControllers()">
                    @foreach($modules as $key=>$val)
                        <option value="{{ $key }}" @if( $rule->module == $key ) selected="selected" @endif >{{ $key }}</option>
                    @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label>@ufl('strings.controller') (* - for all)</label>

                    <select id="rules-edit-form-controller" class="form-control" name="form[controller]" onchange="appAccessCheckController()"></select>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>@ufl('strings.method') (* - for all)</label>
                    <input id="rules-edit-form-method" class="form-control" name="form[method]" value="{{ empty($rule->method) ? '*' : $rule->method }}" />
                </div>

                <div class="form-group">
                    <label>@ufl('strings.access')</label>

                    <select class="form-control" name="form[level]">
                    @foreach(\App\Models\Users\Group::all() as $group)
                        <option value="{{ $group->level }}" @if( $rule->level == $group->level ) selected="selected" @endif >{{ $group->name }}</option>
                    @endforeach
                    </select>
                </div>
                
            </div>
            
            <div class="col-md-12" style="padding-top: 10px;">
                <button type="submit" class="btn btn-primary">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    &nbsp;
                    Сохранить
                </button>
                
                <a href="{{ url('/rules') }}" class="btn btn-default">
                    <span class="glyphicon glyphicon-ban-circle"></span>
                    &nbsp;
                    Отменить
                </a>
            </div>
        </form>
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
var appAccessCurController = '{{ $rule->controller }}';
var appAccessCurModule     = '{{ $rule->module }}';
var appAccessModules        = {!! json_encode($modules) !!};

var appAccessSetControllers = function() {
    var module = $('#rules-edit-form-module').val();
    
    if( !_app.empty(module) ) {
        $('#rules-edit-form-controller').html('');
        
        $.each(appAccessModules, function(n, e) {
            if( n == module ) {
                $('#rules-edit-form-controller').append(
                    $('<option/>', {value: '*', text: '*'})
                );
                
                $.each(e, function(ntx, item) {
                    var option = '<option value="' + item + '"';
                    
                    if( module == appAccessCurModule && appAccessCurController == item ) {
                        option = option + ' selected="selected"';
                    }
                    
                    option = option + '>' + item + '</option>';
                    
                    $('#rules-edit-form-controller').append( option );
                });
            }
        });
    }
};

var appAccessCheckController = function() {
    if( $('#rules-edit-form-controller').val() == '*' ) {
        $('#rules-edit-form-method').val('*');
        $('#rules-edit-form-method').prop({disabled: true});
        
    } else if( $('#rules-edit-form-method').is(':disabled') ) {
        $('#rules-edit-form-method').prop({disabled: false});
        $('#rules-edit-form-method').val('');
    }
};

$(function(){ appAccessSetControllers(); });
</script>
@endpush