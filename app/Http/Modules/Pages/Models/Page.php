<?php namespace App\Http\Modules\Pages\Models;

class Page extends \App\Model
{
    protected $fillable = [
        'uri', 'user_id', 'title', 'keywords', 'default', 
        'description', 'published', 'content',
    ];
    
    protected $casts      = [
        'published' => 'boolean',
    ];

    protected $validation = [
        'title'     => 'required|string|max:255',
        'uri'       => 'required|regex:/[a-z0-9\-]/s|max:255',
        'content'   => 'string',
        'published' => 'numeric',
        'default'   => 'numeric',
    ];
    
    public static function byUri( $uri = null )
    {
        return !empty($uri) ? self::whereUri($uri)->published()->first() : null;
    }
    
    public function scopePublished( $query )
    {
        return $query->wherePublished(1);
    }
    
    public function scopeDefault( $query )
    {
        return $query->whereDefault(1);
    }
    
    public function author()
    {
        return $this->belongsTo( \App\User::class, 'user_id' );
    }
}