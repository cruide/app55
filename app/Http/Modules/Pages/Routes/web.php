<?php

Route:: group(['prefix' => 'pages', 'namespace' => 'Pages'], function() {
    Route::get('list'          , ['as' => 'pages.list'       , 'uses' => 'Index@getList']);
    Route::get('create'        , ['as' => 'pages.create'     , 'uses' => 'Index@getAdd']);
    Route::post('create'       , ['as' => 'pages.create.save', 'uses' => 'Index@postAdd']);
    Route::get('edit/{id}'     , ['as' => 'pages.edit'       , 'uses' => 'Index@getEdit']);
    Route::post('edit/{id}'    , ['as' => 'pages.edit.save'  , 'uses' => 'Index@postEdit']);
    Route::delete('delete/{id}', ['as' => 'pages.delete'     , 'uses' => 'Index@getDelete']);
});

Route::get('page/{uri}', ['as' => 'page', 'uses' => 'Pages\\Index@getIndex']);
