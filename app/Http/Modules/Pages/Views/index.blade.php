@extends('layout')
@section('content')
<div class="page-header col-md-12">
    <h3>{{ $page->title }}</h3>
</div>

<div class="clearfix"></div>

<hr/>

<div class="row">
    <div class="col-md-12">
        {!! $page->content !!}
    </div>
</div>

@endsection
