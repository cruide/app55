@extends('layout')
@section('content')

<div class="page-header col-md-12">
    <h3 class="pull-left"><span class="glyphicon glyphicon-list-alt"></span> Управление страницами</h3>
    <a href="{{ url('/pages/create') }}" class="btn  btn-sm btn-default pull-right" style="margin-top: 20px;">
        <span class="glyphicon glyphicon-plus"></span>
        &nbsp;
        Создать страницу
    </a>
</div>

<div class="clearfix"></div>

<div class="col-md-12" style="padding-top: 20px;">
    <table class="table table-striped" role="table" id="app-admin-pages-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>@ufl('strings.title')</th>
                <th>URI</th>
                <th>Дата создания</th>
                <th>&nbsp;</th>
            </tr>        
        </thead>
        <tbody>
            @foreach($pages as $page)
            <tr class="app-admin-page-{{ $page->id }}">
                <td>{{ $page->id }}</td>
                <td>{{ $page->title }}</td>
                <td>{{ $page->uri }}</td>
                <td>{{ $page->created_at->format('d.m.Y') }}</td>
                <td>
                    <a href="{{ url('/pages/edit/' . $page->id) }}" style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span></a>
                    &nbsp;
                    <a onclick="appBackPagesDelete('{{ $page->id }}')" style="cursor: pointer;"><span class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@push('styles')
<link rel="stylesheet" href="{{ $js_url }}/datatables/datatables.css" />
@endpush

@push('scripts')
<script src="{{ $js_url }}/datatables/datatables.min.js"></script>
<script type="text/javascript">
var appBackPagesDataTable = null;
var appBackPagesDelete    = function( id ) {
    if( !_app.empty(id) ) {
        _app.confirm('Вы действительно хотите удалить страницу?', 'Удаление страницы', function() {
            _app.jsonGet("{{ url('/pages/delete') }}/" + id, function(r) {
                if( !_app.empty(r.success) && r.success == true ) {
                    appBackPagesDataTable.row( $('.app-admin-page-' + id).closest('tr').get(0) ).remove().draw();
                }
            });
        });
    }
};

$(function() {
    appBackPagesDataTable = $('#app-admin-pages-table').DataTable({
        paging: true,
        lengthChange: false,
        searching: true,
        ordering: true,
        info: true,
        autoWidth: false,
    });
});
</script>
@endpush