@extends('layout')
@section('content')
<div class="page-header col-md-12">
    <h3>
        <i class="fa fa-file-text-o" aria-hidden="true"></i>
        <?php echo ($page->id) ? 'Редактирование страницы' : 'Добавление страницы'; ?>
    </h3>
</div>

<div class="clearfix"></div>

<div class="row" style="padding-top: 20px;">
    <div class="col-md-12">
        <form method="POST" id="page-edit-form">
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            
            <div class="col-md-6">
                <div class="form-group">
                    <label>@ufl('strings.title')</label>
                    <input id="page-edit-form-title" class="form-control" type="text" name="form[title]" value="{{ !empty($old_input['title']) ? $old_input['title'] : $page->title }}" />
                </div>
                
                <div class="checkbox">
                    <label>
                      <input type="checkbox" name="form[default]" value="1" @if( !empty($old_input['default']) || (empty($old_input) && !empty($page->default)) ) checked="checked" @endif /> Главная страница
                    </label>
                </div>
            </div>

            <div class="col-md-6">
                <div class="form-group">
                    <label>URI</label>
                    <input id="page-edit-form-uri" class="form-control" type="text" name="form[uri]" value="{{ !empty($old_input['uri']) ? $old_input['uri'] : $page->uri }}" />
                </div>

                <div class="checkbox">
                    <label>
                      <input type="checkbox" name="form[published]" value="1" @if( !empty($old_input['published']) || (empty($old_input) && !empty($page->published)) ) checked="checked" @endif /> Опубликованно
                    </label>
                </div>
            </div>
            
            <div class="col-md-12">
                <hr />
            </div>
            
            <div class="col-md-12">
                <div class="form-group">
                    <label>Контент</label>
                    
                    <div>
                        <textarea id="page-edit-form-content" name="form[content]" cols="10" rows="5">{{ !empty($old_input['content']) ? $old_input['content'] : $page->content }}</textarea>
                    </div>            
                </div>
            </div>

            <div class="col-md-12">
                <hr />
            </div>
            
            <div class="col-md-12" style="padding-top: 10px;">
                <button type="submit" class="btn btn-primary">
                    <span class="glyphicon glyphicon-floppy-disk"></span>
                    &nbsp;
                    Сохранить
                </button>
                
                <a href="{{ url('/pages/list') }}" class="btn btn-default">
                    <span class="glyphicon glyphicon-ban-circle"></span>
                    &nbsp;
                    Отменить
                </a>
            </div>
        </form>
    </div>
</div>
@endsection

@push('scripts')
<!-- <script src="{{ $js_url }}/tinymce/tinymce.min.js"></script> -->
<script type="text/javascript" src="{{ $js_url }}/ckeditor/ckeditor.js"></script>
<script type="text/javascript">
$(function() {
    @if( empty($page->id) )
    $('#page-edit-form-title').on('keyup keypress', function() {
        $('#page-edit-form-uri').val(
            _app.make_slug( $(this).val() ) 
        );
    });
    @else
    $('#page-edit-form-uri').prop({disabled: true});
    @endif
    
    CKEDITOR.replace('page-edit-form-content', {
        contentsCss: "{{ $css_url }}/bootstrap.min.css",
    });
});
</script>
@endpush