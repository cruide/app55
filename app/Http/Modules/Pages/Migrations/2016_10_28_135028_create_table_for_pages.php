<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Rule;

class CreateTableForPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('uri')->index()->default('');
            $table->string('title')->default('');
            $table->string('keywords')->default('');
            $table->string('description')->default('');
            $table->text('content');
            $table->tinyInteger('published')->index()->default(0);
            $table->tinyInteger('default')->index()->default(0);
            $table->bigInteger('user_id')->index()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'getList'  , 'level' => 80]);
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'getEdit'  , 'level' => 80]);
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'postEdit' , 'level' => 80]);
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'getAdd'   , 'level' => 80]);
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'postAdd'  , 'level' => 80]);
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'getDelete', 'level' => 80]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
