<?php namespace App\Http\Modules\Pages;

use App\Models\Setting;
use App\Http\Modules\Pages\Models\Page;

class Index extends \App\Http\Modules\Controller
{
    public function getIndex( $uri = null )
    {
        if( empty($uri) || !($page = Page::byUri($uri)) ) {
            abort(404);
        }
        
        return view('Pages::index', [
            'page' => $page,
        ]);
    }
    
    public function getList()
    {
        return view('Pages::pages', [
            'pages' => Page::all(),
        ]);
    }
    
    public function getAdd()
    {
        return $this->getEdit();
    }
    
    public function postAdd()
    {
        return $this->postEdit();
    }
    
    public function getEdit( $id = null )
    {
        if( empty($id) || !($page = Page::find($id)) ) {
            $page = new Page();
        }
        
        return view('Pages::edit', [
            'page'      => $page,
            'old_input' => old('form'),
        ]);
    }
    
    public function postEdit( $id = null )
    {
        if( empty($id) || !($page = Page::find($id)) ) {
            $page = new Page();
        }

        $form      = array_trim( request()->get('form') );
        $validator = $page->validator( $form );
        
        if( $validator->fails() ) {
            return redirect()->back()
                             ->withInput( request()->all() )
                             ->withErrors( $validator->errors() );
        }

        $page->fill( $form );
        
        if( empty($page->user_id) ) {
            $page->user_id = auth()->id();
        }
        
        $page->default = !empty($form['default']) ? 1 : 0;
        $page->save();
        
        return redirect('/page/edit/' . $page->id)
                   ->with('message', 'Страница успешно сохранена!');
    }
    
    /**
    * Delete page by ID
    * 
    * @param integer $id
    * @return \Illuminate\Http\JsonResponse
    */
    public function getDelete( $id = null )
    {
        if( empty($id) || !is_numeric($id) || !($page = Page::find($id)) ) {
            return response()->json([
                'success' => false,
                'message' => 'Incorrect page identifier',
            ]);
        }
        
        $page->delete();
        
        return response()->json([
            'success' => true,
            'id'      => $id,
        ]);
    }
}

