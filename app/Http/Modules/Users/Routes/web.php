<?php

Route::group(['prefix' => 'users', 'namespace' => 'Users'], function() {
    Route::get('/', ['as' => 'users', 'uses' => 'Index@getIndex']);
});
