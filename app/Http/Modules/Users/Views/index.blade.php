@extends('layout')
@section('content')

<div class="page-header col-md-12">
    <h3 class="pull-left"><i class="fa fa-users" aria-hidden="true"></i> Пользователи</h3>
    <a href="{{ url('/users/create') }}" class="btn btn-sm btn-default pull-right" style="margin-top: 20px;">
        <span class="glyphicon glyphicon-plus"></span>
        &nbsp;
        Добавить пользователя
    </a>
</div>

<div class="clearfix"></div>

<div class="col-md-12" style="padding-top: 20px;">
    <table class="table table-striped" role="table" id="app-admin-pages-table">
        <thead>
            <tr>
                <th>ID</th>
                <th>@ufl('Users::strings.nicname')</th>
                <th>@ufl('strings.group')</th>
                <th>@ufl('Users::strings.register_date')</th>
                <th>&nbsp;</th>
            </tr>        
        </thead>
        <tbody>
            @foreach($users as $user)
            <tr class="app-admin-user-{{ $user->id }}">
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->group->name }}</td>
                <td>{{ $user->created_at->format('d.m.Y') }}</td>
                <td>
                    <a href="{{ url('/users/edit/' . $user->id) }}" style="cursor: pointer;"><span class="glyphicon glyphicon-pencil"></span></a>
                    &nbsp;
                    <a onclick="appBackUsersDelete('{{ $user->id }}')" style="cursor: pointer;"><span class="glyphicon glyphicon-trash"></span></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@push('styles')
<link rel="stylesheet" href="{{ $js_url }}/datatables/datatables.css" />
@endpush

@push('scripts')
<script src="{{ $js_url }}/datatables/datatables.min.js"></script>
<script type="text/javascript">
var appBackUsersDelete    = function( id ) {
    if( !_app.empty(id) ) {
        _app.confirm('Вы действительно хотите удалить пользователя?', 'Удаление пользователя', function() {
            _app.jsonGet("{{ url('/users/delete') }}/" + id, function(r) {
                if( !_app.empty(r.success) && r.success == true ) {

                }
            });
        });
    }
};
</script>
@endpush