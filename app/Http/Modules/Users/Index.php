<?php namespace App\Http\Modules\Users;

use App\User;

class Index extends \App\Http\Modules\Controller
{
    public function getIndex()
    {
        $user = auth()->user();
        
        return view('Users::index', [
            'users' => User::with('profile')
                           ->whereHas('group', function($query) {
                               $query->where('level', '<', auth()->user()->group->level);
                           })
                           ->orderBy('name', 'ASC')
                           ->paginate(30),
        ]);
    }
}

