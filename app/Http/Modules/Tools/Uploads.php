<?php namespace App\Http\Modules\Tools;

use App\Models\Setting;

class Uploads extends \App\Http\Modules\Controller
{
    public function getIndex()
    {
        return '';
    }
    
    /**
    * Return file list for Inagebrowser CKEditor plugin
    * Return JSON result
    */
    public function getImages()
    {
        $files = [];

        foreach(\Storage::disk('images')->files() as $f) {
            $files[] = [
                'image' => url("/uploads/images/{$f}"),
            ];
        }

        return response()->json( $files );
    }
    
    /**
    * Uploading file form uploadimage CKEditor pugin
    * Return JSON result
    */
    public function postImages()
    {
        $post = request()->all();
        
        if( !($image = request()->file('upload')) || !$image->isValid() ) {
            return response()->json([
                'uploaded' => false,
                'error'    => [
                    'message' => 'Не правильный загружаемый файл'
                ]
            ]);
        }

        $extension = $image->getClientOriginalExtension();
        $filename  = generate_file_name(".{$extension}");
        
        if( !$image->move( public_path('/uploads/images'), $filename) ) {
            return response()->json([
                'uploaded' => false,
                'error'    => [
                    'message' => 'Не удалось загрузить файл...'
                ]
            ]);
        }
        
        if( array_key_isset('CKEditorFuncNum', $post) ) {
            return view('uploads.images', [
                'image_url'       => url("/uploads/images/{$filename}"),
                'CKEditorFuncNum' => $post['CKEditorFuncNum'],
            ]);
        }
        
        return response()->json([
            'uploaded' => true,
            'fileName' => $filename,
            'url'      => url("/uploads/images/{$filename}"),
        ]);
    }
}

