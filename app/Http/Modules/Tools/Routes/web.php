<?php

Route::group(['prefix' => 'tools', 'namespace' => 'Tools', 'middleware' => 'auth'], function() {
    Route::get('uploads/images' , ['as' => 'tools.uploads.images'     , 'uses' => 'Uploads@getImages']);
    Route::post('uploads/images', ['as' => 'tools.uploads.images.post', 'uses' => 'Uploads@postImages']);
});