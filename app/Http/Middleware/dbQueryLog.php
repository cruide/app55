<?php namespace App\Http\Middleware;

class dbQueryLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if( env('DB_LOG') === true ) {
            \DB::enableQueryLog();
            
            $response = $next( $request );
            $queries  = [];
            $app_dir  = storage_path() . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR;

            foreach(\DB::getQueryLog() as $log) {
                $queries[] = $log['query'] . " ({$log['time']})";
            }
            
            file_put_contents($app_dir . 'queries' . microtime() . '.log', implode("\n", $queries));
        
            return $response;
        }
        
        return $next( $request ); 
    }
}
