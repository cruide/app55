<?php namespace App\Http\Middleware;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $current_locale = app()->getLocale();
        $locale         = session()->has('locale') ? session()->get('locale') : null;

        if( empty($locale) ) {
            $locale = $request->cookie('locale', env('APP_LOCALE', 'ru'));
        }
        
        if( !empty($locale) && strlen($locale) == 2 && $current_locale != $locale ) {
            if( $locale == 'ru' ) {
                setlocale(LC_ALL, 'ru_RU.UTF-8');
            }
            
            \App::setLocale( $locale );
            \Carbon\Carbon::setLocale( $locale );
        }
        
        return $next($request);
    }
}
