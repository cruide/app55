<?php namespace App\Http\Middleware;

use App\Models\Rule;

class Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle( \Illuminate\Http\Request $request, \Closure $next )
    {
        $action_name = $request->route()->getActionName();

        if( !\Schema::hasTable('rules') || empty($action_name) || $action_name == 'Closure' ) {
            return $next($request);
        }

        list($module, $controller) = explode('\\', str_replace('App\\Http\\Modules\\', '', $action_name));
        
        if( !empty($controller) ) {
            list($controller, $method) = explode('@', $controller);
            
            if( empty($method) ) {
                $method = '*';
            }
            
        } else {
            $controller = $method = '*';
        }
        
        $rule = Rule::whereModule($module)
                    ->whereController($controller)
                    ->whereMethod($method)
                    ->first();
        
        if( !$rule && $method != '*' ) {
            $rule = Rule::whereModule($module)
                        ->whereController($controller)
                        ->whereMethod('*')
                        ->first();
            
        }

        if( !$rule && $controller != '*' ) {
            $rule = Rule::whereModule($module)
                        ->whereController('*')
                        ->whereMethod('*')
                        ->first();
            
        }
        
        if( $rule && $rule->level > 0 ) {
            if( !auth()->check() ) {
                abort(404);
            }
            
            if( auth()->user()->hasLevel($rule->level) ) {
                return $next($request);
            }
            
            abort(401);
        }
        
        return $next($request);
    }
}
