<?php if( !defined('_APP_HELPERS_') ) {
	define('_APP_HELPERS_', true);

	if( !defined('DIR_SEP') ) {
		define('DIR_SEP', DIRECTORY_SEPARATOR);
	}

/* ------------------------------------------------------------------------------ */

	/**
	* Alias for \Illuminate\Support\Str::ucfirst method
	*/
	if( !function_exists('str_ucfirst') ) {
		function str_ucfirst( $str ) {
			return \Illuminate\Support\Str::ucfirst( $str );
		}
	}

	/**
	* Alias for \Illuminate\Support\Str::length method
	*/
	if( !function_exists('str_length') ) {
		function str_length( $str ) {
			return \Illuminate\Support\Str::length( $str );
		}
	}

	/**
	* Alias for \Illuminate\Support\Str::lower method
	*/
	if( !function_exists('str_lower') ) {
		function str_lower( $str ) {
			return \Illuminate\Support\Str::lower( $str );
		}
	}

	/**
	* Alias for \Illuminate\Support\Str::upper method
	*/
	if( !function_exists('str_upper') ) {
		function str_upper( $str ) {
			return \Illuminate\Support\Str::upper( $str );
		}
	}
/* ------------------------------------------------------------------------------ */
	/**
	 * Упрощенный вызов языкового метода
	 *
	 * @param string $_
	 * @return string
	 */
	function l($_, $values = [])
	{
		if( \Lang::has($_) ) {
			return \Lang::get($_, $values);
		}

		return ':' . $_ . ':';
	}
/* ------------------------------------------------------------------------------ */
	/**
	 * Проверка наличия языковой строки
	 *
	 * @param mixed $_
	 */
	function l_has($_)
	{
		return \Lang::has($_);
	}
/* ------------------------------------------------------------------------------ */
	/**
	 * Совмещенные функции для удобства
	 *
	 * @param string $_
	 * @return string
	 */
	function ufl($_, $values = [])
	{
		return str_ucfirst( l($_, $values) );
	}
/* ------------------------------------------------------------------------------ */
	/**
	*  str limit через удаление середины строки
	*
	* @param  string $string
	* @param  int $length
	* @param  string $replacement
	* @return string
	*/
	function str_truncate_middle($string, $length, $replacement = '...', $charset = 'UTF-8')
	{
		if( function_exists('mb_strlen') ) {
			$string_len = mb_strlen($string, $charset);

			if( $string_len <= $length ) {
				return $string;
			}

			$offset       = round( $length / 2 );
			$string_first = mb_substr($string, 0, $offset, $charset);
			$string_last  = mb_substr($string, $string_len - $offset, $offset, $charset);

			return $string_first . $replacement . $string_last;
		}

		$textLength = strlen($string);

		if ($textLength > $length) {

			$start = $length / 2;

			$result = mb_substr($string, 0, $start);
			$result .= $replacement;

			if ($length > 0) {

				$result .= mb_substr($string, -$start);
			}

			return $result;
		} else {
			return $string;
		}
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Более быстрая альтернатива для array_key_exists
	* isset проверяет переменную напрямую в памяти,
	* а array_key_exists перебирает в цикле весь массив
	* Если массив большой и ключ где нибудь в конце,
	* будет долго искать.
	* А так, сразу проверим наличие данных по прямому запросу,
	* а если не сработает isset, ищем с помощью array_key_exists
	*
	* @param string $key
	* @param bool $_array
	* @return bool
	*/
	function array_key_isset($key, $_array)
	{
	  return ( is_array($_array) && (isset($_array[ $key ]) || array_key_exists($key, $_array)) );
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Укороченная альтернатива проверки AJAX запроса.
	*/
	function is_ajax()
	{
		return \Request::ajax();
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Функция для перемешивания первого уровня массива
	* сохраняя при этом связку ключ=>значение
	*
	* @param [] $in
	* @return []
	*/
	function shuffle_assoc( array $in )
	{
		$_ = [];

		while( !empty($in) ) {
			$key       = array_rand($in);
			$_[ $key ] = $in[ $key ];

			unset( $in[ $key ] );
		}

		return $_;
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Salt generation function
	*
	* @return string $lenght
	*/
	function salt_generation( $lenght = 18 )
	{
		return substr( sha1(mt_rand()), 0, $lenght );
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Password generation function
	*
	* @param int $number
	*/
	function password_generate( $number = 8 )
	{
		$arr = [
			'a','b','c','d','e','f','g','h','i','j','k','l',
			'm','n','o','p','r','s','t','u','v','x','y','z',
			'A','B','C','D','E','F','G','H','I','J','K','L',
			'M','N','O','P','R','S','T','U','V','X','Y','Z',
			'1','2','3','4','5','6','7','8','9','0',
		];

		$pass = '';

		for($i = 0; $i < $number; $i++) {
			$pass .= $arr[ rand(0, count($arr) - 1) ];
		}

		return $pass;
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Generate a unique key
	*/
	function uuid()
	{
		return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
			mt_rand( 0, 0x0fff ) | 0x4000,
			mt_rand( 0, 0x3fff ) | 0x8000,
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
		);
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Checking the unique key
	*
	* @param string $uuid
	*/
	function is_uuid($uuid)
	{
		return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Forced cleaning memory
	*/
	function memory_clear()
	{
		if( function_exists('gc_collect_cycles') ) {
			gc_enable();
			$_ = gc_collect_cycles();
			gc_disable();

			return (int)$_;
		}

		return 0;
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Create random file name
	*
	* @param string $extension
	*/
	function generate_file_name($extension)
	{
		return time() . substr( md5(microtime()), 0, rand(5, 12) ) . $extension;
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Get file extension from file name
	*
	* @param string $filename
	*/
	function get_file_extension( $filename )
	{
		return substr( strrchr($filename, '.'), 1 );
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Check image file
	*
	* @param string $filepath
	*/
	function is_image($filepath)
	{
		if( is_file($filepath) && function_exists('getimagesize') ) {
			$img = getimagesize($filepath);

			if( !empty($img['mime']) ) {
				return true;
			}
		}

		return false;
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Get image extension by image type
	*
	* @param mixed $filepath
	* @return mixed
	*/
	function get_image_extension($filepath)
	{
		if( is_file($filepath) && function_exists('getimagesize') ) {
			$img = getimagesize($filepath);

			switch($img['mime']) {
				case 'image/jpeg': return 'jpg';
				case 'image/gif':  return 'gif';
				case 'image/png':  return 'png';
				case 'image/x-ms-bmp': return 'bmp';
			}
		}

		return pathinfo($filepath, PATHINFO_EXTENSION);
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Check for error for Validator errors into templates
	*
	* @param string $field_name
	* @return bool
	*/
	function is_error( $field_name )
	{
		$errors = session('errors');

		if( $errors && $errors->first($field_name) ) {
			return true;
		}

		return false;
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Helper for Carbon
	*
	* @param string $datetime
	* @param string $format
	* @return mixed
	*/
	function carbon( $datetime, $format = null )
	{
		return !empty($format)
				   ? \Carbon\Carbon::parse( $datetime )->format($format)
					   : \Carbon\Carbon::parse( $datetime );
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Check for route
	*/
	function is_route_path( $uri )
	{
		if( !($route = request()->route()) ) {
			return false;
		}

		return ($route->uri() == $uri);
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Trim for array recursive
	*
	* @param array $items
	*
	* @return array
	*/
	function array_trim( array $items, $recursive = true )
	{
		foreach($items as $key=>$item) {
			if( is_array($item) && $recursive ) {
				$items[ $key ] = array_trim($item);

			} else if( is_scalar($item) ) {
				$items[ $key ] = trim($item);

			} else {
				$items[ $key ] = $item;
			}
		}

		return $items;
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Return array of modules with controllers
	*/
	function get_modules()
	{
		static $modules;

		if( empty($modules) ) {
			$path    = app_path('Http/Modules') . DIRECTORY_SEPARATOR;
			$modules = [];

			foreach(@glob("{$path}*", GLOB_ONLYDIR) as $value) {
				$module = str_replace($path, '', $value);

				if( $module != 'Auth' ) {
					if(  !array_key_isset($module, $modules) ) {
						$modules[ $module ] = [];
					}

					foreach(@glob($value . DIRECTORY_SEPARATOR . '*') as $val) {
						if( is_file($val) ) {
							$modules[ $module ][] = str_replace([$value . DIRECTORY_SEPARATOR, '.php'], '', $val);
						}
					}
				}
			}

			unset($value, $path, $val, $module);
			ksort($modules);
		}

		return $modules;
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Check for module
	*
	* @param string $module
	*/
	function module_exists( $module )
	{
		return array_key_isset($module, get_modules());
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Check for controller into module
	*
	* @param string $module
	* @param string $controller
	*/
	function controller_exists($module, $controller)
	{
		$modules = get_modules();

		return (array_key_isset($module, $modules) && in_array($controller, $modules[$module]));
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Check user level
	*
	* @param integer $level
	*/
	function is_user_level( $level )
	{
		return auth()->check() ? auth()->user()->hasLevel($level) : false;
	}
/* ------------------------------------------------------------------------------ */
	/**
	* Helper for auth user check
	*/
	if( !function_exists('is_auth') ) {
		function is_auth()
		{
			return auth()->check();
		}
	}
}
