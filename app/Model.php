<?php namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as EloquentModel;

abstract class Model extends EloquentModel
{
    public static $cache_data;
    
    /**
    * Получение объекта через кеш
    * Аналог ::find() , только через кеш
    * Так же подгружает релейшены
    * 
    * @param mixed $id
    * @return Being;
    */
    public static function findWithCache( $id )
    {
        $class_name = get_called_class();

        if( !is_array(self::$cache_data) ) {
            self::$cache_data = [];
        }
        
        if( empty(self::$cache_data[ $class_name ]) ) {
            self::$cache_data[ $class_name ] = [];
        }
        
        if( !empty(self::$cache_data[ $class_name ][$id]) ) {
            return self::$cache_data[ $class_name ][ $id ];
        }
        
        return self::$cache_data[ $class_name ][ $id ] = self::find($id);
    }

	public function scopeOrderByRandom(Builder $query)
	{
		return $query->orderByRaw('RANDOM()');
	}

	public function scopeWhereLike(Builder $query, $fieldname, $str)
	{
		return $query->whereRaw("CAST(`{$fieldname}` AS CHAR) LIKE '%{$str}%'");
	}

	public function scopeOrWhereLike(Builder $query, $fieldname, $str)
	{
		return $query->orWhereRaw("CAST(`{$fieldname}` AS CHAR) LIKE '%{$str}%'");
	}
	
    public function scopeWhereFulltextMatch(Builder $query, $fieldname, $searchtext)
    {
        if( is_array($fieldname) ) {
            $fieldname = implode(',', $fieldname);
        }
        
        return $query->whereRaw("MATCH({$fieldname}) AGAINST(? IN BOOLEAN MODE)", ["*{$searchtext}*"]);
    }
    
	public function jsonSerialize()
	{
		return method_exists($this, 'transform') ? $this->transform() : $this->toArray(); 
	}
}

