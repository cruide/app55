# Default application on Laravel 5.5 PHP Framework with HMVC

## About

Данное приложение разрабатывается для быстрого создания новых веб-приложений с использованием Laravel 5.5.

В данном приложении реализована в простом виде структура HMVC, что позволяет делать веб-приложение более структурированным и масштабируемым.

Так же реализована система прав доступа к методам контроллеров учитывая уровень доступа группы пользователя.

Приложение находится на стадии разработки и возможно вообще не выйдет в свет...

## Laravel official documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## License

The Default application for Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
