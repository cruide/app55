<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Users\Group;

class CreateUsersTable extends Migration
{
    protected $groups = [
        99 => 'Superusers',
        80 => 'Administrators',
        60 => 'Moderators',
        50 => 'Editors',
        40 => 'Advanced users',
        30 => 'Users',
        10 => 'Noobs',
        0  => 'Guests',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id')->unsigned();
            $table->string('name');
            $table->integer('group_id')->unsigned()->index()->default(0);
            $table->string('email')->unique()->default('');
            $table->string('password');

            $table->rememberToken();
            $table->timestamps();
            $table->index(['remember_token']);
        });
        
        Schema::create('user_profiles', function($table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('user_id')->unsigned()->index()->default(0);
            $table->string('first_name')->default('');
            $table->string('middle_name')->default('');
            $table->string('last_name')->default('');
            $table->date('birthday');
            $table->tinyInteger('gender')->unsigned()->index()->default(1);
            $table->string('avatar_file')->default('');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('email')->index()->default('');
            $table->string('token')->index()->default('');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('user_groups', function( $table ) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('parent_id')->unsigned()->index()->default(0);
            $table->string('name', 50)->index()->default('');
            $table->string('description')->index()->default('');
            $table->smallInteger('level')->unsigned()->index()->default(0);
            $table->tinyInteger('protected')->unsigned()->index()->default(0);
            $table->string('codename')->index()->default('');
            $table->timestamps();
            $table->softDeletes();
        });

        foreach($this->groups as $level=>$name) {
            $group = Group::create([
                'name'        => $name,
                'description' => $name . ' group',
                'level'       => $level,
                'codename'    => \Illuminate\Support\Str::snake($name),
                'protected'   => 1,
            ]);

            if( $group ) {
                $group->protected = 1;
                $group->save();
            }
        }

        \App\User::create([
            'name'     => 'Superuser',
            'email'    => 'root@laravel.cms',
            'password' => bcrypt('toor'),
            'group_id' => 1,
        ]);
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('user_profiles');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('user_groups');
    }
}
