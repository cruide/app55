<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Rule;

class CreateAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rules', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('module', 128)->index()->default('');
            $table->string('controller', 128)->index()->default('*');
            $table->string('method', 128)->index()->default('*');
            $table->smallInteger('level')->unsigned()->index()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
        
        Rule::create(['module' => 'Profile', 'controller' => '*'      , 'method' => '*', 'level' => 10]);

        Rule::create(['module' => 'Rules'  , 'controller' => '*'      , 'method' => '*', 'level' => 99]);
        Rule::create(['module' => 'Tools'  , 'controller' => 'Uploads', 'method' => '*', 'level' => 80]);
        Rule::create(['module' => 'Users'  , 'controller' => 'Index'  , 'method' => '*', 'level' => 80]);
        
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'getList'  , 'level' => 80]);
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'getEdit'  , 'level' => 80]);
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'postEdit' , 'level' => 80]);
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'getAdd'   , 'level' => 80]);
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'postAdd'  , 'level' => 80]);
        Rule::create(['module' => 'Pages', 'controller' => 'Index', 'method' => 'getDelete', 'level' => 80]);
        
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'getList'  , 'level' => 50]);
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'getEdit'  , 'level' => 50]);
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'postEdit' , 'level' => 50]);
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'getAdd'   , 'level' => 50]);
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'postAdd'  , 'level' => 50]);
        Rule::create(['module' => 'News', 'controller' => 'Index', 'method' => 'getDelete', 'level' => 50]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('access');
    }
}
